/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.ulaval.glo2004.InterfaceUtilisateur;

import ca.ulaval.glo2004.domaine.Drawing.TableDrawer;

import java.awt.*;
import java.awt.geom.AffineTransform;
import javax.swing.*;
import javax.swing.border.BevelBorder;


public class DrawingPanel extends JPanel {
    public MainWindow mainWindow;
    public Dimension initialDimension;
    public double zoomFactor = 1;
    public double prevZoomFactor = 1;
    public boolean zoomer;
    private double xOffset = 0;
    private double yOffset = 0;

    public double getxOffset() {
        return xOffset;
    }

    public double getyOffset() {
        return yOffset;
    }

    public void setxOffset(double xOffset){
        this.xOffset = xOffset;
    }

    public void setyOffset(double yOffset){
        this.yOffset = yOffset;
    }

    public DrawingPanel() {

    }

    public DrawingPanel(MainWindow mainWindow) {
        super();
        this.mainWindow = mainWindow;
        setBorder(new javax.swing.border.BevelBorder(BevelBorder.LOWERED));
        setVisible(true);
    }

    public double getZoomFactor() {
        return zoomFactor;
    }

    public void setZoomFactor(double factor) {
        if (((factor < this.zoomFactor && factor >= 0.15) || factor > this.zoomFactor) && factor <= 5){
            this.zoomFactor = factor;
        }
        this.zoomer = true;
    }

    public void resetZoom(){
        this.zoomFactor = 1;
        this.prevZoomFactor = 1;
        this.xOffset = 0;
        this.yOffset = 0;
    }

    /*
    * Solution trouvée sur stackOverflow :
    * https://stackoverflow.com/questions/6543453/zooming-in-and-zooming-out-within-a-panel?answertab=active
    * */

    @Override
    protected void paintComponent(Graphics graphics) {

        if (mainWindow != null) {
            super.paintComponent(graphics);
            Graphics2D g2d = (Graphics2D) graphics;
            if (zoomer) {
                AffineTransform at = new AffineTransform();
                double zoomDiv = zoomFactor / prevZoomFactor;

                xOffset += (1 - zoomDiv) * (mainWindow.cursorPoint.x - xOffset);
                yOffset += (1 - zoomDiv) * (mainWindow.cursorPoint.y - yOffset);

                at.translate(xOffset, yOffset);

                at.scale(zoomFactor, zoomFactor);
                zoomer = false;
                g2d.setTransform(at);

                zoomer = false;
                prevZoomFactor = zoomFactor;
            }
            TableDrawer mainDrawer = new TableDrawer(mainWindow.tableBillardControleur, initialDimension);
            mainWindow.changerGrille();
            mainDrawer.draw(graphics);

        }
    }
}
