/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.ulaval.glo2004.InterfaceUtilisateur;

import ca.ulaval.glo2004.domaine.Table.Boule;
import ca.ulaval.glo2004.domaine.Table.ElementDTO;
import ca.ulaval.glo2004.domaine.Table.TableBillardControleur;
import ca.ulaval.glo2004.domaine.Utilitaires.Conversion;
import ca.ulaval.glo2004.domaine.Utilitaires.Unite;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

public class MainWindow extends javax.swing.JFrame {

    public TableBillardControleur tableBillardControleur;
    private static final String MODE_SIMULATION = "Mode simulation";
    private static final String MODE_EDITION = "Mode édition";
    private static final String ARRETER_SIMULATION = "Arrêter simulation";
    private static final String DEMARRER_SIMULATION = "Démarrer simulation";

    private static final String ACTION_AJOUTER = "Ajouter";
    private static final String ACTION_SELECTIONNER = "Sélectionner";
    private static final String ACTION_SUPPRIMER = "Supprimer";
    private static final String ACTION_BRISER = "Briser";

    private static final String ELEMENT_OBSTACLE = "Obstacle";
    private static final String ELEMENT_POCHE = "Poche";
    private static final String ELEMENT_BOULE = "Boule";
    private static final String ELEMENT_BOULE_BLANCHE = "Boule blanche";
    private boolean estChangeParLeCode = false;
    private boolean estMetrique = false;
    private ModeAplication modeCourant;
    private Unite unite = Unite.POUCES;
    public Point cursorPoint = new Point(0, 0);
    private boolean modeSimulationActif = false;
    private TimerTask task;

    public enum ModeAplication {
        SELECTION, AJOUTER, SUPPRIMER, BRISER
    }

    public MainWindow() {
        tableBillardControleur = new TableBillardControleur();
        initComponents();
        modeCourant = ModeAplication.AJOUTER;
        changerGrille();
        initialiserForceSlider();
        task = getTask();
        task = getTask();
        repaint();
    }

    public void changerGrille() {
        double echelleGrille;
        if (!estMetrique) {
            echelleGrille = echelleGrille_textArea.getText().equals("") ? 0 : Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(Conversion.ConvertirFractionnaireEnDecimal(echelleGrille_textArea.getText())));
        }else {
            echelleGrille = echelleGrille_textArea.getText().equals("") ? 0 : Conversion.CentimetreVersPixel(Double.parseDouble(echelleGrille_textArea.getText()));
        }
        double largeurGrille = drawingPanel.getSize().getWidth() / drawingPanel.getZoomFactor();
        double hauteurGrille = drawingPanel.getSize().getHeight() / drawingPanel.getZoomFactor();
        tableBillardControleur.changerGrille(largeurGrille, hauteurGrille, echelleGrille, grille_checkBox.isSelected());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        uniteGroup = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        creerTable_button = new javax.swing.JButton();
        drawingPanel = new ca.ulaval.glo2004.InterfaceUtilisateur.DrawingPanel(this);
        metrique_radioButton = new javax.swing.JRadioButton();
        imperial_radioButton = new javax.swing.JRadioButton();
        buttonChangerMode = new javax.swing.JButton();
        actions_comboBox = new javax.swing.JComboBox<>(new String[]{ACTION_AJOUTER, ACTION_SELECTIONNER, ACTION_SUPPRIMER, ACTION_BRISER});
        elements_ComboBox = new javax.swing.JComboBox<>(new String[]{ELEMENT_BOULE, ELEMENT_BOULE_BLANCHE, ELEMENT_OBSTACLE, ELEMENT_POCHE});
        jLabel2 = new javax.swing.JLabel();
        txtNbMurs = new javax.swing.JTextField();
        txtLargMurs = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtlongMurs = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        grille_checkBox = new javax.swing.JCheckBox();
        echelleGrille_textArea = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        angle_spinner = new javax.swing.JSpinner();
        longueur_textField = new javax.swing.JTextField();
        largeur_textField = new javax.swing.JTextField();
        diametre_textField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        positionX_spinner = new javax.swing.JSpinner();
        jLabel10 = new javax.swing.JLabel();
        positionY_spinner = new javax.swing.JSpinner();
        appliquer_button = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        force_slider = new javax.swing.JSlider();
        angleSimulation_spinner = new javax.swing.JSpinner();
        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        resetZoom_button = new javax.swing.JButton();
        ouvrirTable = new javax.swing.JButton();
        toggleSimulation_button = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        btn_undo = new javax.swing.JButton();
        btn_redo = new javax.swing.JButton();
        modifierTable = new javax.swing.JButton();
        topMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        openMenuItem = new javax.swing.JMenuItem();
        loadTable = new javax.swing.JMenuItem();
        saveTable = new javax.swing.JMenuItem();
        takePicture = new javax.swing.JMenuItem();
        quitMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("PoolMania");
        setMinimumSize(new java.awt.Dimension(718, 430));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });

        creerTable_button.setText("Créer table");
        creerTable_button.setMargin(new java.awt.Insets(2, 10, 2, 10));
        creerTable_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                creerTable_buttonActionPerformed(evt);
            }
        });

        drawingPanel.setBackground(new java.awt.Color(204, 204, 204));
        drawingPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        drawingPanel.setForeground(new java.awt.Color(204, 204, 204));
        drawingPanel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                drawingPanelMouseDragged(evt);
            }
        });
        drawingPanel.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                drawingPanelMouseWheelMoved(evt);
            }
        });
        drawingPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                drawingPanelMousePressed(evt);
            }

            public void mouseReleased(java.awt.event.MouseEvent evt) {
                drawingPanelMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout drawingPanelLayout = new javax.swing.GroupLayout(drawingPanel);
        drawingPanel.setLayout(drawingPanelLayout);
        drawingPanelLayout.setHorizontalGroup(
                drawingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 672, Short.MAX_VALUE)
        );
        drawingPanelLayout.setVerticalGroup(
                drawingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 0, Short.MAX_VALUE)
        );

        uniteGroup.add(metrique_radioButton);
        metrique_radioButton.setText("Centimètres");
        metrique_radioButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                metrique_radioButtonMouseClicked(evt);
            }
        });

        uniteGroup.add(imperial_radioButton);
        imperial_radioButton.setSelected(true);
        imperial_radioButton.setText("Pouces");
        imperial_radioButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imperial_radioButtonMouseClicked(evt);
            }
        });

        buttonChangerMode.setText("Mode simulation");
        buttonChangerMode.setMaximumSize(new java.awt.Dimension(130, 25));
        buttonChangerMode.setMinimumSize(new java.awt.Dimension(130, 25));
        buttonChangerMode.setPreferredSize(new java.awt.Dimension(130, 25));
        buttonChangerMode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonChangerModeActionPerformed(evt);
            }
        });

        actions_comboBox.setSelectedItem(ACTION_AJOUTER);
        actions_comboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actions_comboBoxActionPerformed(evt);
            }
        });

        jLabel2.setText("Nb murs");

        txtNbMurs.setText("6");
        txtNbMurs.setName("nbMurs"); // NOI18N
        txtNbMurs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNbMursActionPerformed(evt);
            }
        });

        txtLargMurs.setText("2");
        txtLargMurs.setName("nbMurs"); // NOI18N
        txtLargMurs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLargMursActionPerformed(evt);
            }
        });

        jLabel3.setText("Largeur murs");

        txtlongMurs.setText("25");
        txtlongMurs.setName("nbMurs"); // NOI18N
        txtlongMurs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtlongMursActionPerformed(evt);
            }
        });

        jLabel4.setText("Longueur murs");

        grille_checkBox.setText("Grille magnétique");
        grille_checkBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                grille_checkBoxMouseClicked(evt);
            }
        });
        grille_checkBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                grille_checkBoxActionPerformed(evt);
            }
        });

        echelleGrille_textArea.setText("2");
        echelleGrille_textArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                echelleGrille_textAreaActionPerformed(evt);
            }
        });
        echelleGrille_textArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                echelleGrille_textAreaKeyTyped(evt);
            }
        });

        jLabel5.setText("Échelle :");

        jLabel8.setText("Longueur : ");

        jLabel11.setText("Largeur :");

        jLabel12.setText("Diametre :");

        jLabel13.setText("Angle :");

        angle_spinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                angle_spinnerStateChanged(evt);
            }
        });

        longueur_textField.setText("0");

        largeur_textField.setText("0");
        largeur_textField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                largeur_textFieldActionPerformed(evt);
            }
        });

        diametre_textField.setText("0");

        jLabel9.setText("X :");

        positionX_spinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                positionX_spinnerStateChanged(evt);
            }
        });

        jLabel10.setText("Y :");

        positionY_spinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                positionY_spinnerStateChanged(evt);
            }
        });

        appliquer_button.setText("Appliquer");
        appliquer_button.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                appliquer_buttonMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addComponent(jLabel8)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(longueur_textField))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(appliquer_button))
                                        .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel12)
                                                        .addComponent(jLabel11)
                                                        .addComponent(jLabel13)
                                                        .addComponent(jLabel9)
                                                        .addComponent(jLabel10))
                                                .addGap(8, 8, 8)
                                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(positionY_spinner)
                                                        .addComponent(positionX_spinner)
                                                        .addComponent(angle_spinner, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
                                                        .addGroup(jPanel3Layout.createSequentialGroup()
                                                                .addGap(3, 3, 3)
                                                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(diametre_textField, javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addComponent(largeur_textField))))))
                                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel8)
                                        .addComponent(longueur_textField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel11)
                                        .addComponent(largeur_textField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(7, 7, 7)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel12)
                                        .addComponent(diametre_textField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(appliquer_button)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel13)
                                        .addComponent(angle_spinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel9)
                                        .addComponent(positionX_spinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel10)
                                        .addComponent(positionY_spinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap())
        );

        jTabbedPane1.addTab("Caractéristiques", jPanel3);

        force_slider.setName("sliderForceSimulation"); // NOI18N
        force_slider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                force_sliderStateChanged(evt);
            }
        });

        angleSimulation_spinner.setName("SpinnerAngleSimulation"); // NOI18N
        angleSimulation_spinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                angleSimulation_spinnerStateChanged(evt);
            }
        });

        jLabel1.setText("Angle :");

        jLabel6.setText("Force :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(force_slider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(24, 24, 24)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabel6)
                                                        .addComponent(jLabel1))
                                                .addGap(18, 18, 18)
                                                .addComponent(angleSimulation_spinner)))
                                .addContainerGap(15, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(angleSimulation_spinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel1))
                                .addGap(8, 8, 8)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(force_slider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(139, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Simulation", jPanel1);

        resetZoom_button.setText("Réinitialiser zoom");
        resetZoom_button.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                resetZoom_buttonMouseClicked(evt);
            }
        });

        ouvrirTable.setText("Ouvir table");
        ouvrirTable.setMargin(new java.awt.Insets(2, 10, 2, 10));
        ouvrirTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ouvrirTableActionPerformed(evt);
            }
        });

        toggleSimulation_button.setText("Démarrer simulation");
        toggleSimulation_button.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                toggleSimulation_buttonMouseClicked(evt);
            }
        });

        jButton1.setText("jButton1");

        btn_undo.setText("Undo");
        btn_undo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_undoMouseClicked(evt);
            }
        });
        btn_undo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_undoActionPerformed(evt);
            }
        });

        btn_redo.setText("Redo");
        btn_redo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_redoMouseClicked(evt);
            }
        });
        btn_redo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_redoActionPerformed(evt);
            }
        });

        modifierTable.setText("Modifier table");
        modifierTable.setMargin(new java.awt.Insets(2, 10, 2, 10));
        modifierTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierTableActionPerformed(evt);
            }
        });

        fileMenu.setText("File");

        openMenuItem.setText("Open");
        fileMenu.add(openMenuItem);

        loadTable.setText("Ouvrir une table");
        loadTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadTableActionPerformed(evt);
            }
        });
        fileMenu.add(loadTable);

        saveTable.setText("Sauvegarder la table");
        saveTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveTableActionPerformed(evt);
            }
        });
        fileMenu.add(saveTable);

        takePicture.setText("Exporter l'image");
        takePicture.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                takePictureActionPerformed(evt);
            }
        });
        fileMenu.add(takePicture);

        quitMenuItem.setText("Quit");
        quitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(quitMenuItem);

        topMenuBar.add(fileMenu);

        editMenu.setText("Edit");
        topMenuBar.add(editMenu);

        setJMenuBar(topMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(15, 15, 15)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                .addGroup(layout.createSequentialGroup()
                                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                .addComponent(jLabel3)
                                                                                .addComponent(jLabel4))
                                                                        .addGap(40, 40, 40))
                                                                .addComponent(grille_checkBox))
                                                        .addComponent(txtLargMurs, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(txtlongMurs, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jLabel2)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(txtNbMurs, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addGroup(layout.createSequentialGroup()
                                                                        .addGap(17, 17, 17)
                                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                .addComponent(creerTable_button, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addComponent(ouvrirTable, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addGroup(layout.createSequentialGroup()
                                                                                        .addComponent(jLabel5)
                                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                        .addComponent(echelleGrille_textArea, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                .addComponent(modifierTable, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                .addComponent(metrique_radioButton, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(imperial_radioButton))))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(14, 14, 14)
                                                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(42, 42, 42)
                                                .addComponent(btn_undo)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(btn_redo)))
                                .addGap(41, 41, 41)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(drawingPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(actions_comboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(elements_ComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(resetZoom_button)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(toggleSimulation_button)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(buttonChangerMode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(buttonChangerMode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(actions_comboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(elements_ComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(resetZoom_button)
                                        .addComponent(toggleSimulation_button)
                                        .addComponent(btn_undo)
                                        .addComponent(btn_redo))
                                .addGap(3, 3, 3)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jTabbedPane1)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel2)
                                                        .addComponent(txtNbMurs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel3)
                                                .addGap(1, 1, 1)
                                                .addComponent(txtLargMurs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jLabel4)
                                                .addGap(1, 1, 1)
                                                .addComponent(txtlongMurs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(grille_checkBox)
                                                .addGap(2, 2, 2)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel5)
                                                        .addComponent(echelleGrille_textArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addComponent(creerTable_button)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(modifierTable)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(ouvrirTable)
                                                .addGap(18, 18, 18)
                                                .addComponent(metrique_radioButton)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(imperial_radioButton))
                                        .addComponent(drawingPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void initialiserForceSlider() {
        force_slider.setMaximum(100);
        force_slider.setMinimum(0);
        this.tableBillardControleur.setForceQueue(force_slider.getValue());
        drawingPanel.setBorder(null);
        toggleSimulation_button.setEnabled(false);
    }

    private void metrique_radioButtonActionPerformed(ActionEvent evt) {
    }

    private TimerTask getTask() {
        return task = new TimerTask() {

            @Override
            public void run() {
                tableBillardControleur.etapeSimulation();
                drawingPanel.setZoomFactor(drawingPanel.getZoomFactor());
                drawingPanel.repaint();
            }
        };
    }

    private void quitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_quitMenuItemActionPerformed

    private void drawingPanelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drawingPanelMousePressed
        /*<<<<<<< HEAD
        cursorPoint = evt.getPoint();

        double vraiX, vraiY;

        Point clic = new Point();
        double y = drawingPanel.getyOffset();
        double x = drawingPanel.getxOffset();
        vraiY = -y + cursorPoint.getY();
        vraiX = -x + cursorPoint.getX();

        clic.setLocation(vraiX / drawingPanel.zoomFactor, vraiY / drawingPanel.zoomFactor);
        switch (modeCourant) {
            case AJOUTER:
                switch (elements_ComboBox.getSelectedItem().toString()) {
                    case ELEMENT_BOULE:
                        tableBillardControleur.ajouterBouleNumerote((Point) clic.clone());
                        break;
                    case ELEMENT_OBSTACLE:
                        tableBillardControleur.ajouterUnObstacle((Point) clic.clone());
                        break;
                    case ELEMENT_POCHE:
                        tableBillardControleur.ajouterUnePoche((Point) clic.clone());
                        break;
                    case ELEMENT_BOULE_BLANCHE:
                        tableBillardControleur.ajouterBouleBlanche((Point) clic.clone());
                }
======= */
        if (!modeSimulationActif || (modeCourant == ModeAplication.AJOUTER && elements_ComboBox.getSelectedItem().toString().equals(ELEMENT_BOULE_BLANCHE))) {
            cursorPoint = evt.getPoint();

            double vraiX, vraiY;

            Point clic = new Point();
            double y = drawingPanel.getyOffset();
            double x = drawingPanel.getxOffset();
            vraiY = -y + cursorPoint.getY();
            vraiX = -x + cursorPoint.getX();

            clic.setLocation(vraiX / drawingPanel.zoomFactor, vraiY / drawingPanel.zoomFactor);
            switch (modeCourant) {
                case AJOUTER:
                    switch (elements_ComboBox.getSelectedItem().toString()) {
                        case ELEMENT_BOULE:
                            tableBillardControleur.ajouterBouleNumerote((Point) clic.clone());
                            break;
                        case ELEMENT_OBSTACLE:
                            tableBillardControleur.ajouterUnObstacle((Point) clic.clone());
                            break;
                        case ELEMENT_POCHE:
                            tableBillardControleur.ajouterUnePoche((Point) clic.clone());
                            break;
                        case ELEMENT_BOULE_BLANCHE:
                            tableBillardControleur.ajouterBouleBlanche((Point) clic.clone());
                    }

                    break;
                case SUPPRIMER:
                    //supprimer objet
                    tableBillardControleur.selectionnerElement((Point) clic.clone(), unite);
                    tableBillardControleur.supprimerElement();
                    break;
                case SELECTION:
                    ElementDTO elementDTO = tableBillardControleur.selectionnerElement((Point) clic.clone(), unite);
                    afficherCaracteristiques(elementDTO);
                    break;
                case BRISER:
                    ElementDTO elementDTO2 = tableBillardControleur.briserMurs((Point) clic.clone());
                    afficherCaracteristiques(elementDTO2);
                    break;
            }
            drawingPanel.setZoomFactor(drawingPanel.getZoomFactor());//corriger le zoom qui se reset (╯▔皿▔)╯
            drawingPanel.repaint();
        }

    }//GEN-LAST:event_drawingPanelMousePressed

    private void afficherCaracteristiques(ElementDTO elementDTO) {
        estChangeParLeCode = true;
        if (elementDTO != null) {
            positionX_spinner.setValue(elementDTO.getX());
            positionY_spinner.setValue(elementDTO.getY());
            if (elementDTO.getLongueur() != null) {
                longueur_textField.setEnabled(true);
                longueur_textField.setText(String.valueOf(elementDTO.getLongueur()));
            } else {
                longueur_textField.setText("0");
                longueur_textField.setEnabled(false);
            }

            if (elementDTO.getLargeur() != null) {
                largeur_textField.setEnabled(true);
                largeur_textField.setText(String.valueOf(elementDTO.getLargeur()));
            } else {
                largeur_textField.setText("0");
                largeur_textField.setEnabled(false);
            }
            if (elementDTO.getAngle() != null) {
                angle_spinner.setEnabled(true);
                angle_spinner.setValue(elementDTO.getAngle());
            } else {
                angle_spinner.setValue(0);
                angle_spinner.setEnabled(false);
            }

            if (elementDTO.getDiametre() != null) {
                diametre_textField.setEnabled(true);
                diametre_textField.setText(String.valueOf(elementDTO.getDiametre()));
            } else {
                diametre_textField.setText("0");
                diametre_textField.setEnabled(false);
            }
        }
        estChangeParLeCode = false;
    }

    private void creerTable_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_creerTable_buttonActionPerformed
        tableBillardControleur.creerTable(Integer.parseInt(txtNbMurs.getText()), Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(Conversion.ConvertirFractionnaireEnDecimal(txtlongMurs.getText()))), Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(Conversion.ConvertirFractionnaireEnDecimal(txtLargMurs.getText()))), null, null, null);
        drawingPanel.setZoomFactor(drawingPanel.getZoomFactor());
        drawingPanel.repaint();
    }//GEN-LAST:event_creerTable_buttonActionPerformed


    private void buttonChangerModeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonChangerModeActionPerformed
        String texteAffichage = buttonChangerMode.getText().equals(MODE_SIMULATION) ? MODE_EDITION : MODE_SIMULATION;
        buttonChangerMode.setText(texteAffichage);
        toggleSimulation_button.setEnabled(texteAffichage.equals(MODE_EDITION));
        this.modeSimulationActif = texteAffichage.equals(MODE_EDITION);

        if (buttonChangerMode.getText().equals(MODE_SIMULATION)) {
            this.jTabbedPane1.setSelectedIndex(0);
        } else {
            this.jTabbedPane1.setSelectedIndex(1);
        }

        tableBillardControleur.setModeSimulationActive(texteAffichage.equals(MODE_EDITION));
        drawingPanel.setZoomFactor(drawingPanel.getZoomFactor());
        drawingPanel.repaint();
    }//GEN-LAST:event_buttonChangerModeActionPerformed

    private void drawingPanelMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_drawingPanelMouseWheelMoved
        cursorPoint = evt.getPoint();
        double x = drawingPanel.getxOffset();
        double y = drawingPanel.getyOffset();

        //Zoom in
        if (evt.getWheelRotation() < 0) {
            drawingPanel.setZoomFactor(drawingPanel.getZoomFactor() * 1.1);
            changerGrille();
            drawingPanel.repaint();
        }
        //Zoom out
        if (evt.getWheelRotation() > 0) {
            drawingPanel.setZoomFactor(drawingPanel.getZoomFactor() / 1.1);
            changerGrille();
            drawingPanel.repaint();
        }

        //drawingPanel.setxOffset(x-cursorPoint.getX());
        //drawingPanel.setyOffset(y-cursorPoint.getY());
        /*double x = drawingPanel.getxOffset() * Math.pow(drawingPanel.getZoomFactor(), evt.getWheelRotation());
        double y = drawingPanel.getyOffset() * Math.pow(drawingPanel.getZoomFactor(), evt.getWheelRotation());

        Point2D newZoom = new Point((int)Math.round(x),(int)Math.round(y));

        drawingPanel.setxOffset(drawingPanel.getxOffset() + Math.round(Math.round((1 - (newZoom.getX() / drawingPanel.getxOffset())) * (cursorPoint.x - drawingPanel.getxOffset()))));
        drawingPanel.setyOffset(drawingPanel.getyOffset() + Math.round(Math.round((1 - (newZoom.getY() / drawingPanel.getyOffset())) * (cursorPoint.y - drawingPanel.getyOffset()))));

        //drawingPanel.setZoomFactor(newZoom);*/
    }//GEN-LAST:event_drawingPanelMouseWheelMoved

    private void metrique_radioButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_metrique_radioButtonMouseClicked
        if (!estMetrique) {
            estMetrique = true;
            unite = Unite.CENTIMETRES;
            double longueur = Math.round(Conversion.ConvertirEnMetrique(Conversion.ConvertirFractionnaireEnDecimal(longueur_textField.getText())));
            double largeur = Math.round(Conversion.ConvertirEnMetrique(Conversion.ConvertirFractionnaireEnDecimal(largeur_textField.getText())));
            double diametre = Math.round(Conversion.ConvertirEnMetrique(Conversion.ConvertirFractionnaireEnDecimal(diametre_textField.getText())));
            double echelleGrille = Math.round(Conversion.ConvertirEnMetrique(Conversion.ConvertirFractionnaireEnDecimal(echelleGrille_textArea.getText())));
            longueur_textField.setText(String.valueOf(longueur));
            largeur_textField.setText(String.valueOf(largeur));
            diametre_textField.setText(String.valueOf(diametre));
            echelleGrille_textArea.setText(String.valueOf(echelleGrille));
            miseAJourValeurs();
        }
    }//GEN-LAST:event_metrique_radioButtonMouseClicked

    private void imperial_radioButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imperial_radioButtonMouseClicked
        if (estMetrique) {
            estMetrique = false;
            unite = Unite.POUCES;
            longueur_textField.setText(String.valueOf(Conversion.ConvertirEnImperial(Double.valueOf(longueur_textField.getText()))));
            largeur_textField.setText(String.valueOf(Conversion.ConvertirEnImperial(Double.valueOf(longueur_textField.getText()))));
            diametre_textField.setText(String.valueOf(Conversion.ConvertirEnImperial(Double.valueOf(diametre_textField.getText()))));
            echelleGrille_textArea.setText(String.valueOf(Conversion.ConvertirEnImperial(Double.valueOf(echelleGrille_textArea.getText()))));
            miseAJourValeurs();
        }

    }//GEN-LAST:event_imperial_radioButtonMouseClicked

    private void txtNbMursActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNbMursActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNbMursActionPerformed

    private void txtLargMursActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLargMursActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLargMursActionPerformed

    private void txtlongMursActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtlongMursActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtlongMursActionPerformed

    private void grille_checkBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_grille_checkBoxActionPerformed
        drawingPanel.setZoomFactor(drawingPanel.getZoomFactor());
        changerGrille();
        drawingPanel.repaint();
    }//GEN-LAST:event_grille_checkBoxActionPerformed

    private void echelleGrille_textAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_echelleGrille_textAreaActionPerformed
        // TODO add your handling code here:
        if (!echelleGrille_textArea.getText().equals("")) {
            drawingPanel.setZoomFactor(drawingPanel.getZoomFactor());
            changerGrille();
            drawingPanel.repaint();
        }
    }//GEN-LAST:event_echelleGrille_textAreaActionPerformed

    private void grille_checkBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_grille_checkBoxMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_grille_checkBoxMouseClicked

    private void actions_comboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actions_comboBoxActionPerformed
        switch (actions_comboBox.getSelectedItem().toString()) {
            case ACTION_AJOUTER:
                modeCourant = ModeAplication.AJOUTER;
                break;
            case ACTION_SELECTIONNER:
                modeCourant = ModeAplication.SELECTION;
                break;
            case ACTION_SUPPRIMER:
                modeCourant = ModeAplication.SUPPRIMER;
                break;
            case ACTION_BRISER:
                modeCourant = ModeAplication.BRISER;
                break;
        }

    }//GEN-LAST:event_actions_comboBoxActionPerformed

    private void largeur_textFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_largeur_textFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_largeur_textFieldActionPerformed

    private void positionY_spinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_positionY_spinnerStateChanged
        miseAJourValeurs();
    }//GEN-LAST:event_positionY_spinnerStateChanged

    private void echelleGrille_textAreaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_echelleGrille_textAreaKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_echelleGrille_textAreaKeyTyped

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        drawingPanel.setZoomFactor(drawingPanel.getZoomFactor());
        changerGrille();
        drawingPanel.repaint();
    }//GEN-LAST:event_formComponentResized

    private void positionX_spinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_positionX_spinnerStateChanged
        miseAJourValeurs();
    }//GEN-LAST:event_positionX_spinnerStateChanged

    private void appliquer_buttonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_appliquer_buttonMouseClicked
        miseAJourValeurs();
    }//GEN-LAST:event_appliquer_buttonMouseClicked

    private void resetZoom_buttonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_resetZoom_buttonMouseClicked
        drawingPanel.resetZoom();
        drawingPanel.repaint();
    }//GEN-LAST:event_resetZoom_buttonMouseClicked

    private void takePictureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_takePictureActionPerformed
        //Icon blueIcon = new ImageIcon("yourFile.gif");
        Object stringArray[] = {"PNG", "JPG", "Annuler"};
        int result = JOptionPane.showOptionDialog(this, "Type d'image:", "Choisir une option",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, stringArray,
                stringArray[0]);
        if (result != 2) {
            BufferedImage buffImg = new BufferedImage(this.drawingPanel.getWidth(), this.drawingPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics g = buffImg.createGraphics();
            this.drawingPanel.paint(g);
            g.dispose();
            File file;
            String type;
            switch (result) {
                case 0:
                    file = new File("img.png");
                    type = "png";
                    break;
                case 1:
                    file = new File("img.jpg");
                    type = "jpg";
                    break;
                default:
                    return;
            }
            try {
                ImageIO.write(buffImg, type, file);
                //ImageIO.write(buffImg, "jpg", file);
            } catch (IOException ex) {
                Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_takePictureActionPerformed

    private void angle_spinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_angle_spinnerStateChanged
        miseAJourValeurs();
    }//GEN-LAST:event_angle_spinnerStateChanged

    private void saveTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveTableActionPerformed
        this.tableBillardControleur.saveTable();
    }//GEN-LAST:event_saveTableActionPerformed

    private void loadTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadTableActionPerformed
        this.tableBillardControleur.loadTable();
        drawingPanel.repaint();
    }//GEN-LAST:event_loadTableActionPerformed

    private void ouvrirTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ouvrirTableActionPerformed
        this.tableBillardControleur.loadTable();
        drawingPanel.repaint();
    }//GEN-LAST:event_ouvrirTableActionPerformed

    private void drawingPanelMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drawingPanelMouseDragged
        if (!modeSimulationActif) {
            cursorPoint = evt.getPoint();
            double vraiX, vraiY;
            Point clic = new Point();

            double xOffset = drawingPanel.getxOffset();
            double yOffset = drawingPanel.getyOffset();
            vraiX = -xOffset + cursorPoint.getX();
            vraiY = -yOffset + cursorPoint.getY();

            clic.setLocation(vraiX / drawingPanel.zoomFactor, vraiY / drawingPanel.zoomFactor);
            if (modeCourant == ModeAplication.SELECTION) {

                positionX_spinner.setValue(clic.getX());
                positionY_spinner.setValue(clic.getY());

                miseAJourValeurs();
                drawingPanel.repaint();
            }
        }
    }//GEN-LAST:event_drawingPanelMouseDragged

    private void force_sliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_force_sliderStateChanged
        this.tableBillardControleur.setForceQueue(force_slider.getValue());
        drawingPanel.setZoomFactor(drawingPanel.getZoomFactor());
        drawingPanel.repaint();
    }//GEN-LAST:event_force_sliderStateChanged

    private void angleSimulation_spinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_angleSimulation_spinnerStateChanged
        this.tableBillardControleur.setAngleQueue(Double.parseDouble(angleSimulation_spinner.getValue().toString()));
        drawingPanel.setZoomFactor(drawingPanel.getZoomFactor());
        drawingPanel.repaint();
    }//GEN-LAST:event_angleSimulation_spinnerStateChanged

    private void toggleSimulation_buttonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_toggleSimulation_buttonMouseClicked
        // TODO faire la simulation
        String texteAffichage = toggleSimulation_button.getText().equals(DEMARRER_SIMULATION) ? ARRETER_SIMULATION : DEMARRER_SIMULATION;
        toggleSimulation_button.setText(texteAffichage);
        Timer timer = new Timer();

        if (texteAffichage.equals(ARRETER_SIMULATION)) {
            //Démarrer la simulation
            System.out.println("Démarrer la simulation");
            Boule boule = tableBillardControleur.getBoule()[0];
            double x = Math.cos(Math.toRadians(tableBillardControleur.getQueue().getAngle())) * (tableBillardControleur.getQueue().getForce() + 10);
            double y = Math.sin(Math.toRadians(tableBillardControleur.getQueue().getAngle())) * (tableBillardControleur.getQueue().getForce() + 10);
            tableBillardControleur.demarrerSimulation(x, y);
            task = getTask();
            timer.schedule(task, 0, 1000 / 24);

        } else {
            //Arreter la simulation
            System.out.println("Arreter la simulation");

            task.cancel();
            timer.cancel();
            tableBillardControleur.arreterSimulation();

        }


    }//GEN-LAST:event_toggleSimulation_buttonMouseClicked

    private void btn_redoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_redoMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_redoMouseClicked

    private void btn_undoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_undoMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_undoMouseClicked

    private void modifierTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierTableActionPerformed
        this.tableBillardControleur.modifyTable(Integer.parseInt(txtNbMurs.getText()), Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(Conversion.ConvertirFractionnaireEnDecimal(txtlongMurs.getText()))), Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(Conversion.ConvertirFractionnaireEnDecimal(txtLargMurs.getText()))));
        drawingPanel.repaint();
    }//GEN-LAST:event_modifierTableActionPerformed

    private void btn_undoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_undoActionPerformed
        this.tableBillardControleur.undo();
        drawingPanel.setZoomFactor(drawingPanel.getZoomFactor());
        drawingPanel.repaint();
    }//GEN-LAST:event_btn_undoActionPerformed

    private void btn_redoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_redoActionPerformed
        this.tableBillardControleur.redo();
        drawingPanel.setZoomFactor(drawingPanel.getZoomFactor());
        drawingPanel.repaint();
    }//GEN-LAST:event_btn_redoActionPerformed

    private void drawingPanelMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drawingPanelMouseReleased
        if (grille_checkBox.isSelected() && !modeSimulationActif) {
            cursorPoint = evt.getPoint();
            double vraiX, vraiY;
            Point clic = new Point();

            double xOffset = drawingPanel.getxOffset();
            double yOffset = drawingPanel.getyOffset();
            vraiX = -xOffset + cursorPoint.getX();
            vraiY = -yOffset + cursorPoint.getY();

            clic.setLocation(vraiX / drawingPanel.zoomFactor, vraiY / drawingPanel.zoomFactor);
            if (modeCourant == ModeAplication.SELECTION) {
                Point snapPoint = tableBillardControleur.getGrilleMagnetique().snapGrille(clic);

                positionX_spinner.setValue(snapPoint.getX());
                positionY_spinner.setValue(snapPoint.getY());

                miseAJourValeurs();
                drawingPanel.repaint();
            }
        }
    }//GEN-LAST:event_drawingPanelMouseReleased

    private void miseAJourValeurs() {
        if (estChangeParLeCode == false) {

            double longueur = 0;
            double largeur = 0;
            double diametre = 0;
            if (unite == Unite.POUCES) {
                longueur = Conversion.ConvertirEnMetrique(Conversion.ConvertirFractionnaireEnDecimal(longueur_textField.getText()));
                largeur = Conversion.ConvertirEnMetrique(Conversion.ConvertirFractionnaireEnDecimal(largeur_textField.getText()));
                diametre = Conversion.ConvertirEnMetrique(Conversion.ConvertirFractionnaireEnDecimal(diametre_textField.getText()));
            } else {
                try {
                    longueur = Double.parseDouble(longueur_textField.getText());
                    largeur = Double.parseDouble(largeur_textField.getText());
                    diametre = Double.parseDouble(diametre_textField.getText());
                } catch (NumberFormatException e) {
                    System.out.println("Erreur de conversion");
                }

            }

            ElementDTO elementDTO = new ElementDTO(Double.valueOf(positionX_spinner.getValue().toString()), Double.valueOf(positionY_spinner.getValue().toString()), longueur,
                    largeur, Double.valueOf(angle_spinner.getValue().toString()), diametre);

            tableBillardControleur.miseAJourValeurs(elementDTO, unite);
            drawingPanel.setZoomFactor(drawingPanel.getZoomFactor());
            drawingPanel.repaint();
        }
    }

    /**
     * Permet de faire la conversion d'un nombre fractionnaire en impérial vers
     * centimètre vers pixel
     *
     * @return
     */
    private double conversionFractionnairePixel() {
        return 0;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox actions_comboBox;
    private javax.swing.JSpinner angleSimulation_spinner;
    private javax.swing.JSpinner angle_spinner;
    private javax.swing.JButton appliquer_button;
    private javax.swing.JButton btn_redo;
    private javax.swing.JButton btn_undo;
    private javax.swing.JButton buttonChangerMode;
    private javax.swing.JButton creerTable_button;
    private javax.swing.JTextField diametre_textField;
    private ca.ulaval.glo2004.InterfaceUtilisateur.DrawingPanel drawingPanel;
    private javax.swing.JTextField echelleGrille_textArea;
    private javax.swing.JMenu editMenu;
    private javax.swing.JComboBox<String> elements_ComboBox;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JSlider force_slider;
    private javax.swing.JCheckBox grille_checkBox;
    private javax.swing.JRadioButton imperial_radioButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField largeur_textField;
    private javax.swing.JMenuItem loadTable;
    private javax.swing.JTextField longueur_textField;
    private javax.swing.JRadioButton metrique_radioButton;
    private javax.swing.JButton modifierTable;
    private javax.swing.JMenuItem openMenuItem;
    private javax.swing.JButton ouvrirTable;
    private javax.swing.JSpinner positionX_spinner;
    private javax.swing.JSpinner positionY_spinner;
    private javax.swing.JMenuItem quitMenuItem;
    private javax.swing.JButton resetZoom_button;
    private javax.swing.JMenuItem saveTable;
    private javax.swing.JMenuItem takePicture;
    private javax.swing.JButton toggleSimulation_button;
    private javax.swing.JMenuBar topMenuBar;
    private javax.swing.JTextField txtLargMurs;
    private javax.swing.JTextField txtNbMurs;
    private javax.swing.JTextField txtlongMurs;
    private javax.swing.ButtonGroup uniteGroup;
    // End of variables declaration//GEN-END:variables
}
