package ca.ulaval.glo2004.domaine.Drawing;

import ca.ulaval.glo2004.domaine.Table.*;
import ca.ulaval.glo2004.domaine.Utilitaires.Conversion;

import java.awt.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;

public class TableDrawer {
    private final TableBillardControleur controller;
    private Dimension initialDimension;

    public TableDrawer(TableBillardControleur controller, Dimension initialDimension) {
        this.controller = controller;
        this.initialDimension = initialDimension;
    }

    public void draw(Graphics g) {
        if (controller.getFormePolygone() != null) {
            drawFond(g);
        }

        drawPoche(g);
        drawBoule(g);
        drawMurs(g);
        drawObstacles(g);
        drawLignesGrilleMagnetique(g);
        drawCoins(g);
        if (controller.isModeSimulationActive()){
            drawContourBlanche(g);
        }


    }

    private void drawContourBlanche(Graphics g) {
        Boule[] boules = controller.getBoule();

        for (int i = 0; i < boules.length; i++) {
            if (boules[i] != null && boules[i].isEstBouleBlanche()) {
                Boule boule = boules[i];

                Graphics2D graphics2D = (Graphics2D) g.create();
                graphics2D.setColor(new Color(225,225,225, 125));
                graphics2D.setStroke(new BasicStroke(4));
                graphics2D.drawOval((int)boule.getForme().getX()-4, (int)boule.getForme().getY()-4, (int) boule.getForme().getBounds2D().getWidth() + 8, (int) boule.getForme().getBounds2D().getWidth()+8);

                Line2D line = new Line2D.Double((int)boule.getForme().getX() + (int)Math.round(boule.getForme().getBounds2D().getWidth()/2),(int)boule.getForme().getY()+ (int)Math.round(boule.getForme().getBounds2D().getHeight()/2),(int)boule.getForme().getX() + (int)Math.round(boule.getForme().getBounds2D().getWidth()/2)+45+(200*this.controller.getQueue().getForce()/100), (int)boule.getForme().getY()+ (int)Math.round(boule.getForme().getBounds2D().getHeight()/2));
                AffineTransform at = AffineTransform.getRotateInstance(Math.toRadians(controller.getQueue().getAngle()), line.getX1(), line.getY1());
                graphics2D.setStroke(new BasicStroke(4));
                graphics2D.draw(at.createTransformedShape(line));
            }
        }
    }

    private void drawCoins(Graphics g) {
        List<Coin> coins = controller.getCoins();


        for (Coin coin : coins) {

            Graphics2D graphics2D = (Graphics2D) g.create();

            graphics2D.setColor(Color.red);
            graphics2D.draw(coin.getHitbox());
            graphics2D.dispose();

        }
    }

    private void drawBoule(Graphics g) {
        Boule[] boules = controller.getBoule();

        for (int i = 0; i < boules.length; i++) {
            if (boules[i] != null) {
                Boule boule = boules[i];

                g.drawImage(boule.getImage(), (int) boule.getForme().getX(),
                        (int)boule.getForme().getY(), (int) boule.getForme().getBounds2D().getWidth(), (int) boule.getForme().getBounds2D().getWidth(), null);
            }
        }
    }

    private void drawMurs(Graphics g) {
        List<Mur> murs = controller.getMurs();

        for (Mur mur : murs) {
            Graphics2D graphics2D = (Graphics2D) g.create();
            graphics2D.setColor(mur.getCouleur());
            graphics2D.fill(mur.getPolygon());
            graphics2D.dispose();

        }
    }

    private void drawObstacles(Graphics g) {
        List<Obstacle> obstacles = controller.getObstacles();

        for (Obstacle obstacle : obstacles) {
            Graphics2D graphics2D = (Graphics2D) g.create();
            graphics2D.setColor(obstacle.getCouleur());
            graphics2D.fill(obstacle.getPolygonAvecRotation());
            graphics2D.dispose();

        }
    }

    private void drawPoche(Graphics g) {
        List<Poche> poches = controller.getPoches();

        for (Poche poche : poches) {
           g.setColor(new Color(0,0,0));
           g.fillOval((int)poche.getForme().getX(), (int)poche.getForme().getY(), (int) poche.getForme().getBounds2D().getWidth(), (int) poche.getForme().getBounds2D().getWidth());
        }
    }

    private void drawFond(Graphics g) {
        Graphics2D graphFond = (Graphics2D) g.create();
        graphFond.setColor(new Color(0, 102, 0));
        graphFond.fill(controller.getFormePolygone());
        graphFond.draw(controller.getFormePolygone());
        graphFond.dispose();
    }


    private void drawLignesGrilleMagnetique(Graphics g) {
        GrilleMagnetique grille = controller.getGrilleMagnetique();
        double echelle = grille.getEchelleGrille();
        int largeur = (int) grille.getLargeurGrille();
        int hauteur = (int) grille.getHauteurGrille();

        g.setColor(new Color(0, 0, 0, 90));
        if (grille.estVisible()) {

            for (int x = -1000; x <= largeur; x += echelle) {
                for (int y = -1000; y <= hauteur; y += echelle) {
                    g.drawRect(x, y, (int) Math.round(echelle), (int) Math.round(echelle));
                }
            }
        }
    }
}
