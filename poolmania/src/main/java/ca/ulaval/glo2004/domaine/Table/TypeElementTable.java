package ca.ulaval.glo2004.domaine.Table;

public enum TypeElementTable {
    POCHE,
    BOULE_BLANCHE,
    BOULE_NUMEROTE,
    MUR,
    OBSTACLE,
    COIN
}
