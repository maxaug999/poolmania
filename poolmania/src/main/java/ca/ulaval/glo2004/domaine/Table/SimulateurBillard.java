package ca.ulaval.glo2004.domaine.Table;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.Timer;
import java.util.TimerTask;

public class SimulateurBillard{

    private TableBillard tablePreSimulation;
    private TableBillard tableSimulation;
    private TableBillardControleur controleur;
    private Timer timer;
    private TimerTask task;

    public SimulateurBillard(TableBillard tableBillard, TableBillardControleur controleur){
        this.tablePreSimulation = tableBillard;
        this.tableSimulation = tableBillard;
        this.controleur = controleur;
        this.timer = new Timer();
    }

    public TableBillard getTablePreSimulation() {
        return tablePreSimulation;
    }
    
    public TableBillard avancerSimulation(){
        Boule boule;
        for (int i=0; i < tableSimulation.getListeBoules().length; i++) {
            if (tableSimulation.getListeBoules()[i] == null)
                continue;

            boule = tableSimulation.getListeBoules()[i];

            boule.deplacer();

            //Vérifier s'il y a collision avec une autre boule
            Boule otherBall = tableSimulation.verifierCollisionBoules(boule);
            if (otherBall != null){
                boule.HandleBallCollision(otherBall);
            }

            //Vérifier si une boule tombe dans une poche
            for (int j=0; j < tableSimulation.getListeBoules().length; j++) {
                if (tableSimulation.getListeBoules()[j] != null) {
                    if (tableSimulation.verifierCollisionPoche(tableSimulation.getListeBoules()[j])) {
                        tableSimulation.getListeBoules()[j] = null;
                    }
                }
            }

            //Vérifier si la boule touche un mur
            Mur mur = tableSimulation.verifierCollisionBouleMur(boule);
            if (mur != null){
                boule.SetMouvement(rebondBouleMur(boule.GetMouvement(),  new Point2D.Double(mur.getPolygon().xpoints[0] - mur.getPolygon().xpoints[3], mur.getPolygon().ypoints[0] - mur.getPolygon().ypoints[3])));
            }

        }
        return tableSimulation;
    }

    private Point2D.Double rebondBouleMur(Point.Double vecteurBoule, Point.Double vecteurMur){

        Point2D.Double vecteurNormalMur = vecteurNormal(vecteurMur);

        Point2D.Double nouvVecteur = multiplicationVecteur(vecteurNormalMur, -2*produitScalaire(vecteurNormalMur, vecteurBoule));

        return soustractionVecteur(nouvVecteur, vecteurBoule);
    }

    private double produitScalaire(Point2D.Double vecteur1 , Point2D.Double vecteur2){
        double product = 0;
        product =+ vecteur1.x * vecteur2.x;
        product =+ vecteur1.y * vecteur2.y;
        return product;
    }

    private Point2D.Double vecteurNormal(Point2D.Double vecteur){
        return new Point2D.Double(-vecteur.y/Math.sqrt(vecteur.x*vecteur.x+vecteur.y*vecteur.y), vecteur.x/Math.sqrt(vecteur.x*vecteur.x+vecteur.y*vecteur.y));
    }

    private Point2D.Double multiplicationVecteur(Point2D.Double vecteur1, double facteur){
        return new Point2D.Double(vecteur1.x*facteur, vecteur1.y*facteur);
    }

    private Point2D.Double soustractionVecteur(Point2D.Double vecteur1, Point2D.Double vecteur2){
        return new Point2D.Double(vecteur1.x-vecteur2.x, vecteur1.y-vecteur2.y);
    }
}
