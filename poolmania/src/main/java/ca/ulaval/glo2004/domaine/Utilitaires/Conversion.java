package ca.ulaval.glo2004.domaine.Utilitaires;

public class Conversion {
    public static final double POUCE_PAR_CENTIMETRE = 0.393700787;
    public static final double CENTIMETRE_PAR_POUCE = 2.54;
    public static final double PIXEL_PAR_CENTIMETRE = 4; //Un nombre arbitraire, surement à changer


    public static double ConvertirFractionnaireEnDecimal(String valeurString) {
        String val[] = valeurString.split(" ", 2);
        double entier = Double.parseDouble(val[0]);

        double total = entier;

        if(val.length >= 2) {
            String valFraction[] = val[1].split("/", 2);
            double nominateur = Double.parseDouble(valFraction[0]);
            double denominateur = Double.parseDouble(valFraction[1]);
            double division = nominateur / denominateur;
            total += division;
        }

        return total;
    }
    public static double ConvertirEnMetrique(double valeur) {
        return valeur * CENTIMETRE_PAR_POUCE;
    }
    public static double ConvertirEnImperial(double metrique) {
        return metrique * POUCE_PAR_CENTIMETRE;
    }
    public static double PixelVersCentimetre(int pixel) {
        return pixel / PIXEL_PAR_CENTIMETRE;
    }
    public static int CentimetreVersPixel(double centimetre) {
        return (int) Math.round(centimetre * PIXEL_PAR_CENTIMETRE);
    }
}
