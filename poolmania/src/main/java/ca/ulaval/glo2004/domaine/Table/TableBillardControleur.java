package ca.ulaval.glo2004.domaine.Table;

import ca.ulaval.glo2004.domaine.Drawing.GrilleMagnetique;
import ca.ulaval.glo2004.domaine.Utilitaires.Conversion;
import ca.ulaval.glo2004.domaine.Utilitaires.Unite;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

public class TableBillardControleur {

    TableBillard tableBillard;
    Object elementSelectionne;
    GrilleMagnetique grilleMagnetique;
    private int nbMurs;
    private int longueur_murs;
    private int largeur_murs;
    boolean modeSimulationActive = false;
    Queue queue = new Queue(new Point(0, 0), 0, 0, new Polygon(), new Color(0, 0, 255), new Dimension());
    private SimulateurBillard simulation;

    public void setForceQueue(int force) {
        this.queue.setForce(force);
    }

    public void setAngleQueue(double angle) {
        this.queue.setAngle(angle);
    }

    public Queue getQueue() {
        return queue;
    }

    public void setModeSimulationActive(boolean modeSimulationActive) {
        this.modeSimulationActive = modeSimulationActive;
    }

    public boolean isModeSimulationActive() {
        return modeSimulationActive;
    }

    public TableBillardControleur() {
        this.tableBillard = new TableBillard();
    }

    public List<Mur> getMurs() {
        return tableBillard.getListeMurs();
    }

    public void creerTable(int nombreMurs, int longueur, int largeur, Point[] boulesInitiales, List<Mur> mursInitiaux, List<Poche> pochesInitiales) {
        tableBillard = new TableBillard();
        this.tableBillard.creerTable(nombreMurs, longueur, largeur, boulesInitiales, mursInitiaux, pochesInitiales);
    }

    public void modifyTable(int nombreMurs, int longueur, int largeur) {
        this.tableBillard.modifyTable(nombreMurs, longueur, largeur, true);
    }

    public void saveTable() {
        this.tableBillard.saveTable();
    }

    public void loadTable() {
        this.tableBillard.loadTable();
    }

    public Polygon getFormePolygone() {
        return this.tableBillard.getForme();
    }

    public void setFormePolygone(Polygon polygone) {
        this.tableBillard.setForme(polygone);
    }

    public ElementDTO briserMurs(Point mousePoint) {
        elementSelectionne = tableBillard.trouverElementClic(mousePoint);
        if (elementSelectionne == null) {
            return null;
        }

        ElementDTO elementDTO = null;
        tableBillard.Briser(getNoMur(mousePoint));
        elementDTO = new ElementDTO(mousePoint.getX(), mousePoint.getY(), null, null, null, null);
        return elementDTO;
    }

    public ElementDTO selectionnerElement(Point mousePoint, Unite unite) {
        elementSelectionne = tableBillard.trouverElementClic(mousePoint);
        if (elementSelectionne == null) {
            return null;
        }

        ElementDTO elementDTO = null;
        TypeElementTable typeElement = trouverTypeElementTable();

        switch (typeElement) {
            /*case MUR:
                MurExterne murExterne = (MurExterne) elementSelectionne;
                tableBillard.Briser(getNoMur(mousePoint));
                elementDTO = new ElementDTO(mousePoint.getX(), mousePoint.getY(), null,null,null,null);
                break;*/
            case COIN:
                Coin coin = (Coin) elementSelectionne;
                elementDTO = new ElementDTO(coin.getHitbox().getX(), coin.getHitbox().getY(), null, null, null, null);
                break;
            case BOULE_BLANCHE:
                Boule blanche = (Boule) elementSelectionne;
                elementDTO = new ElementDTO(blanche.getForme().getX(), blanche.getForme().getY(), null, null, null, Conversion.ConvertirEnImperial(tableBillard.getDiametreBoules()));
                break;
            case BOULE_NUMEROTE:
                BouleNumerote boule = (BouleNumerote) elementSelectionne;
                //elementDTO = new ElementDTO(boule.getForme().getX(), boule.getForme().getY(), null, null, null, changementUnites(boule.getDiametre(), unite));
                elementDTO = new ElementDTO(boule.getForme().getX(), boule.getForme().getY(), null, null, null, Conversion.ConvertirEnImperial(tableBillard.getDiametreBoules()));
                break;
            case POCHE:
                Poche poche = (Poche) elementSelectionne;
                elementDTO = new ElementDTO(poche.getForme().getX(), poche.getForme().getY(), null, null, null, Conversion.ConvertirEnImperial(poche.getDiametre()));
                break;
            case OBSTACLE:
                Obstacle obstacle = (Obstacle) elementSelectionne;
                elementDTO = new ElementDTO(obstacle.getPolygonAvecRotation().getBounds2D().getX(), obstacle.getPolygonAvecRotation().getBounds2D().getY(), obstacle.getDimension().getWidth(),
                        obstacle.getDimension().getHeight(), obstacle.getAngle(), null);
                break;
            default:
                return null;

        }
        return elementDTO;
    }

    public void supprimerElement() {
        if (this.elementSelectionne == null) {
            return;
        }

        TypeElementTable typeElement = trouverTypeElementTable();
        if (typeElement != null) {
            tableBillard.supprimerElement(typeElement, this.elementSelectionne);
        }

    }

    //TODO : trouver une meilleure solution ?
    private TypeElementTable trouverTypeElementTable() {
        if (elementSelectionne instanceof Coin) {
            return TypeElementTable.COIN;
        }
        if (elementSelectionne instanceof MurExterne) {
            return TypeElementTable.MUR;
        }
        if (elementSelectionne instanceof BouleNumerote) {
            return TypeElementTable.BOULE_NUMEROTE;
        }
        if (elementSelectionne instanceof Boule) {
            return TypeElementTable.BOULE_BLANCHE;
        }
        if (elementSelectionne instanceof Poche) {
            return TypeElementTable.POCHE;
        }
        if (elementSelectionne instanceof Obstacle) {
            return TypeElementTable.OBSTACLE;
        }

        return null;
    }

    public void ajouterBouleBlanche(Point clic) {
        this.tableBillard.ajouterBouleBlanche(clic);
    }

    public void ajouterBouleNumerote(Point clic) {
        this.tableBillard.ajouterBouleNumerote(clic);
    }

    public List<Coin> getCoins() {
        return tableBillard.getListeCoins();
    }

    public Boule[] getBoule() {
        return tableBillard.getListeBoules();
    }

    public List<Obstacle> getObstacles() {
        return tableBillard.getListeObstacles();
    }

    public void ajouterUnObstacle(Point mousePoint) {
        this.tableBillard.ajouterUnObstacle(mousePoint);
    }

    public TableBillard getTableBillard() {
        return this.tableBillard;
    }

    public GrilleMagnetique getGrilleMagnetique() {
        return this.grilleMagnetique;
    }

    public void ajouterUnePoche(Point mousePoint) {
        tableBillard.ajouterUnePoche(mousePoint);
    }

    public List<Poche> getPoches() {
        return tableBillard.getListePoches();
    }

    public void undo() {
        this.tableBillard.undo();
    }

    public void redo() {
        this.tableBillard.redo();
    }

    public void changerGrille(double largeurGrille, double hauteurGrille, double echelleGrille, boolean estAfficher) {
        if (grilleMagnetique == null) {
            this.grilleMagnetique = new GrilleMagnetique(largeurGrille, hauteurGrille, echelleGrille, estAfficher);
        } else {
            double echelle = echelleGrille;//Conversion.ConvertirEnMetrique(echelleGrille);
            this.grilleMagnetique.setEchelleGrille(echelle);
            this.grilleMagnetique.setLargeurGrille(largeurGrille);
            this.grilleMagnetique.setHauteurGrille(hauteurGrille);
            this.grilleMagnetique.estVisible(estAfficher);
        }
    }

    public void miseAJourValeurs(ElementDTO elementDTO, Unite unite) {
        if (elementSelectionne == null) {
            return;
        }
        TypeElementTable typeElement = trouverTypeElementTable();
        //this.tableBillard.miseAJourValeurs(elementDTO, unite, typeElement, elementSelectionne);
        Double longueur = (double) Conversion.CentimetreVersPixel(elementDTO.getLongueur());
        Double largeur = (double) Conversion.CentimetreVersPixel(elementDTO.getLargeur());
        Double angle = changementUnites(elementDTO.getAngle(), unite);
        Double diametre = elementDTO.getDiametre();
        switch (typeElement) {
            case OBSTACLE:
                Obstacle obstacle = (Obstacle) elementSelectionne;
                obstacle.setAngle(angle);
                if (obstacle.getCoinCollision() == null) {
                    //pas de collision donc modifier l'obstacle normalement
                    obstacle.getPolygon().getBounds().setBounds((int) elementDTO.getX(), (int) elementDTO.getY(), longueur.intValue(), largeur.intValue());
                    //Changer la grosseur du polygon
                    obstacle.getPolygon().xpoints[2] = (int) (obstacle.getPolygon().xpoints[0] + longueur);
                    obstacle.getPolygon().xpoints[3] = (int) (obstacle.getPolygon().xpoints[0] + longueur);
                    obstacle.getPolygon().ypoints[1] = (int) (obstacle.getPolygon().ypoints[0] + largeur);
                    obstacle.getPolygon().ypoints[2] = (int) (obstacle.getPolygon().ypoints[0] + largeur);

                    //Déplacer le polygon
                    double diffX = elementDTO.getX() - obstacle.getPolygon().getBounds().getX();
                    double diffY = elementDTO.getY() - obstacle.getPolygon().getBounds().getY();
                    obstacle.getPolygon().translate((int) diffX, (int) diffY);

                    //Rotation
                    obstacle.faireRotation();
                } else {
                    //collision donc modifier les obstacle
                    Obstacle obstacle1 = (Obstacle) obstacle.getCoinCollision().getMurDroit();
                    Obstacle obstacle2 = (Obstacle) obstacle.getCoinCollision().getMurGauche();

                    int[] xpoints1 = obstacle1.getPolygonAvecRotation().xpoints.clone();
                    int[] ypoints1 = obstacle1.getPolygonAvecRotation().ypoints.clone();
                    int[] xpoints2 = obstacle2.getPolygonAvecRotation().xpoints.clone();
                    int[] ypoints2 = obstacle2.getPolygonAvecRotation().ypoints.clone();

                    double moyX1 = (xpoints1[0] + xpoints1[1] + xpoints1[2] + xpoints1[3]) / 4;
                    double moyY1 = (ypoints1[0] + ypoints1[1] + ypoints1[2] + ypoints1[3]) / 4;
                    double moyX2 = (xpoints2[0] + xpoints2[1] + xpoints2[2] + xpoints2[3]) / 4;
                    double moyY2 = (ypoints2[0] + ypoints2[1] + ypoints2[2] + ypoints2[3]) / 4;

                    double moy1 = (moyX1 + moyY1) / 2;
                    double moy2 = (moyX2 + moyY2) / 2;

                    obstacle1.getPolygon().getBounds().setBounds((int) elementDTO.getX(), (int) elementDTO.getY(), longueur.intValue(), largeur.intValue());
                    obstacle2.getPolygon().getBounds().setBounds((int) elementDTO.getX(), (int) elementDTO.getY(), longueur.intValue(), largeur.intValue());

                    int longPrec = Math.abs(xpoints1[0] - xpoints1[3]);
                    int largPrec = Math.abs(ypoints1[2] - ypoints1[3]);

                    obstacle1.getPolygon().xpoints[2] = (int) (obstacle1.getPolygon().xpoints[0] + longueur);
                    obstacle1.getPolygon().xpoints[3] = (int) (obstacle1.getPolygon().xpoints[0] + longueur);
                    obstacle1.getPolygon().ypoints[1] = (int) (obstacle1.getPolygon().ypoints[0] + largeur);
                    obstacle1.getPolygon().ypoints[2] = (int) (obstacle1.getPolygon().ypoints[0] + largeur);
                    obstacle1.getDimension().setSize(longueur, largeur);

                    obstacle2.getPolygon().xpoints[2] = (int) (obstacle2.getPolygon().xpoints[0] + longueur + (longueur - longPrec));
                    obstacle2.getPolygon().xpoints[3] = (int) (obstacle2.getPolygon().xpoints[0] + longueur + (longueur - longPrec));
                    obstacle2.getPolygon().ypoints[1] = (int) (obstacle2.getPolygon().ypoints[0] + largeur + (largeur - largPrec));
                    obstacle2.getPolygon().ypoints[2] = (int) (obstacle2.getPolygon().ypoints[0] + largeur + (largeur - largPrec));
                    obstacle2.getDimension().setSize(longueur, largeur);

                    if (moyX1 < moyX2 && moyY1 > moyY2) {
                        System.out.println("_l");
                        //Déplacer les polygons
                        /*double diffX1 = elementDTO.getX() - obstacle1.getPolygon().getBounds().getX();
                        double diffY1 = elementDTO.getY() - obstacle1.getPolygon().getBounds().getY();

                        obstacle1.getPolygon().translate((int) diffX1, (int) diffY1);
                        obstacle.getCoinCollision().getHitbox().translate((int) diffX1, (int) diffY1);

                        double diffX2 = elementDTO.getX() - obstacle2.getPolygon().getBounds().getX();
                        double diffY2 = elementDTO.getY() - obstacle2.getPolygon().getBounds().getY();

                        obstacle2.getPolygon().translate((int) diffX2, (int) diffY2);
                        obstacle.faireRotationAutourduCoin();*/
                    } else if (moyX1 > moyX2 && moyY1 > moyY2) {
                        //System.out.println("l_");

                        //Déplacer les polygons
                        double diffX1 = elementDTO.getX() - obstacle1.getPolygon().getBounds().getX();
                        double diffY1 = elementDTO.getY() - obstacle1.getPolygon().getBounds().getY();
                        obstacle1.getPolygon().translate((int) diffX1, (int) diffY1);
                        obstacle.getCoinCollision().getHitbox().translate((int) diffX1, (int) diffY1);
                        double diffX2 = elementDTO.getX() - obstacle2.getPolygon().getBounds().getX();
                        double diffY2 = elementDTO.getY() - obstacle2.getPolygon().getBounds().getY();
                        obstacle2.getPolygon().translate((int) diffX2, (int) diffY2);
                        obstacle.faireRotationAutourduCoin();
                    } else if (moyX1 < moyX2 && moyY1 < moyY2) {
                        System.out.println("^l");

                        //Déplacer les polygons
                        double diffX1 = elementDTO.getX() - obstacle1.getPolygon().getBounds().getX();
                        double diffY1 = elementDTO.getY() - obstacle1.getPolygon().getBounds().getY();
                        obstacle1.getPolygon().translate((int) diffX1, (int) diffY1);
                        obstacle.getCoinCollision().getHitbox().translate((int) diffX1, (int) diffY1);
                        double diffX2 = elementDTO.getX() - obstacle2.getPolygon().getBounds().getX();
                        double diffY2 = elementDTO.getY() - obstacle2.getPolygon().getBounds().getY();
                        obstacle2.getPolygon().translate((int) diffX2, (int) diffY2);
                        obstacle.faireRotationAutourduCoin();
                    } else if (moyX1 > moyX2 && moyY1 < moyY2) {

                        //Déplacer les polygons
                        double diffX1 = elementDTO.getX() - obstacle1.getPolygon().getBounds().getX();
                        double diffY1 = elementDTO.getY() - obstacle1.getPolygon().getBounds().getY();
                        obstacle1.getPolygon().translate((int) diffX1, (int) diffY1);
                        obstacle.getCoinCollision().getHitbox().translate((int) diffX1, (int) diffY1);
                        double diffX2 = elementDTO.getX() - obstacle2.getPolygon().getBounds().getX();
                        double diffY2 = elementDTO.getY() - obstacle2.getPolygon().getBounds().getY();
                        obstacle2.getPolygon().translate((int) diffX2, (int) diffY2);
                        obstacle.faireRotationAutourduCoin();
                    }

                }

                Coin coinCollision = tableBillard.verifierCollisionMur(obstacle);
                if (coinCollision != null) {
                    //Il y a une collision
                    tableBillard.getListeCoins().add(coinCollision);
                }
                break;
            case BOULE_NUMEROTE:
                BouleNumerote bouleNumerote = (BouleNumerote) elementSelectionne;
                bouleNumerote.setPosition(new Point((int) elementDTO.getX(), (int) elementDTO.getY()));
                tableBillard.setDiametreBoules(diametre);
                break;
            case BOULE_BLANCHE:
                Boule boule = (Boule) elementSelectionne;
                boule.setPosition(new Point((int) elementDTO.getX(), (int) elementDTO.getY()));
                tableBillard.setDiametreBoules(diametre);
                break;
            case POCHE:
                //this.tableBillard.pushPoches();
                Poche poche = (Poche) elementSelectionne;
                poche.setDiametre(diametre);
                poche.setPosition(new Point((int) elementDTO.getX(), (int) elementDTO.getY()));

                poche.getForme().setFrame(new Rectangle2D.Double(poche.getPosition().getX(),
                        poche.getPosition().getY(), Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(poche.getDiametre())), Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(poche.getDiametre()))));
                break;
            case MUR:

                break;
            case COIN:
                Coin coin = (Coin) elementSelectionne;

                coin.getMurDroit().getPolygon().xpoints[2] += (int) elementDTO.getX() - coin.getPrevPosition().getX();
                coin.getMurDroit().getPolygon().xpoints[3] += (int) elementDTO.getX() - coin.getPrevPosition().getX();
                coin.getMurGauche().getPolygon().xpoints[0] += (int) elementDTO.getX() - coin.getPrevPosition().getX();
                coin.getMurGauche().getPolygon().xpoints[1] += (int) elementDTO.getX() - coin.getPrevPosition().getX();

                coin.getMurDroit().getPolygon().ypoints[2] += (int) elementDTO.getY() - coin.getPrevPosition().getY();
                coin.getMurDroit().getPolygon().ypoints[3] += (int) elementDTO.getY() - coin.getPrevPosition().getY();
                coin.getMurGauche().getPolygon().ypoints[0] += (int) elementDTO.getY() - coin.getPrevPosition().getY();
                coin.getMurGauche().getPolygon().ypoints[1] += (int) elementDTO.getY() - coin.getPrevPosition().getY();

                coin.setPrevPosition(new Point((int) elementDTO.getX(), (int) elementDTO.getY()));
                Rectangle rectangle = new Rectangle();
                rectangle.setRect(elementDTO.getX(), elementDTO.getY(), 15, 15);
                coin.getHitbox().setRect(rectangle);
                this.tableBillard.setFondTable(tableBillard.getListeMurs().size());

                break;
            default:
                break;
        }
    }

    private Double changementUnites(Double valeur, Unite unite) {
        if (valeur == null) {
            return null;
        }
        //Si l'interface utilisateur est en centimetre pas besoin de convertir
        /*if(unite == Unite.POUCES) {
            valeur = Conversion.ConvertirEnImperial(valeur);
        }*/
        return valeur;
    }

    private int getNoMur(Point point) {
        int noMur = 0, i = 0;
        for (Mur mur : tableBillard.getListeMurs()) {
            Shape shape = (Shape) mur.getPolygon();
            if (shape.contains((Point2D) point)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public void demarrerSimulation(double x, double y) {
        this.tableBillard.getListeBoules()[0].SetMouvement(new Point2D.Double(x, y));
        simulation = new SimulateurBillard(this.tableBillard, this);
    }

    public void etapeSimulation() {
        this.tableBillard = simulation.avancerSimulation();
    }

    public void arreterSimulation() {
        this.tableBillard = simulation.getTablePreSimulation();
    }

}
