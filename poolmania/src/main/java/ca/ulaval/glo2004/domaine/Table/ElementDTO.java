package ca.ulaval.glo2004.domaine.Table;


public class ElementDTO {
    private Double x;
    private Double y;
    private Double longueur;
    private Double largeur;
    private Double angle;
    private Double diametre;

    public ElementDTO(Double x, Double y, Double longueur, Double largeur, Double angle, Double diametre) {
        this.x = x;
        this.y = y;
        this.longueur = longueur;
        this.largeur = largeur;
        this.angle = angle;
        this.diametre = diametre;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Double getLongueur() {
        return longueur;
    }

    public Double getLargeur() {
        return largeur;
    }

    public Double getAngle() {
        return angle;
    }

    public Double getDiametre() {
        return diametre;
    }
}
