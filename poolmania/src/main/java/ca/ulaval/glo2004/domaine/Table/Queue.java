package ca.ulaval.glo2004.domaine.Table;

import java.awt.*;

public class Queue {
    private Point position;
    private double angle;
    private int force;
    private Polygon forme;

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public int getForce() {
        return force;
    }

    public void setForce(int force) {
        this.force = force;
    }

    private Color couleur;
    private Dimension dimension;

    public Queue(Point position, double angle, int force, Polygon forme, Color couleur, Dimension dimension) {
        this.position = position;
        this.angle = angle;
        this.force = force;
        this.forme = forme;
        this.couleur = couleur;
        this.dimension = dimension;
    }

    public void JouerCoup() {

    }
}
