package ca.ulaval.glo2004.domaine.Table;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

public abstract class Mur implements Serializable {
    private Dimension dimension;
    private double angle;
    private Color couleur;
    private Polygon polygon;

    public Mur() {
        super();
    }

    public Mur(double angle, Color couleur, Polygon forme, Dimension dimension) {
        this.dimension = dimension;
        this.angle = angle;
        this.couleur = couleur;
        this.polygon = forme;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public Color getCouleur() {
        return couleur;
    }

    public void setCouleur(Color couleur) {
        this.couleur = couleur;
    }

    public Polygon getPolygon() {
        return polygon;
    }

    public void setPolygon(Polygon polygon) {
        this.polygon = polygon;
    }
}
