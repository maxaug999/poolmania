package ca.ulaval.glo2004.domaine.Utilitaires;

import java.util.ArrayDeque;
import java.util.EventObject;
import java.util.Queue;
import java.util.Stack;

public class UndoRedo {

    public Queue<EventObject> queue;
    public Stack<EventObject> stack;

    public UndoRedo(){
        this.stack = new Stack<>();
        this.queue = new ArrayDeque<>();
    }

    public void setQueue(Queue<EventObject> queue) {
        this.queue = queue;
    }

    public void setStack(Stack<EventObject> stack) {
        this.stack = stack;
    }

    public Queue<EventObject> getQueue() {
        return queue;
    }

    public Stack<EventObject> getStack() {
        return stack;
    }

    public void enqueue(EventObject e){
        this.queue.add(e);
    }

    public void stack(EventObject e){
        this.stack.push(e);
    }
}

