package ca.ulaval.glo2004.domaine.Table;

import ca.ulaval.glo2004.domaine.Utilitaires.Conversion;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

public class Obstacle extends Mur{

    public static final double LONGUEUR_PAR_DEFAUT = 10;
    public static final double HAUTEUR_PAR_DEFAUT = 2;

    private Polygon polygonAvecRotation;

    private Coin coinCollision;

    public Obstacle(Point position) {
        super();



    }

    public Obstacle(double angle, Color couleur, Polygon forme, Dimension dimension) {
        super(angle, couleur, forme, dimension);
        this.setPolygonAvecRotation(forme);
    }

    public Polygon getPolygonAvecRotation() {
        return polygonAvecRotation;
    }

    public void setPolygonAvecRotation(Polygon polygonAvecRotation) {
        this.polygonAvecRotation = polygonAvecRotation;
    }

    public Coin getCoinCollision() {
        return coinCollision;
    }

    public void setCoinCollision(Coin coinCollision) {
        this.coinCollision = coinCollision;
    }

    public void faireRotation(){

        int[] x;
        int[] y;
        int[] rx = new int[4];
        int[] ry = new int[4];
        x = this.getPolygon().xpoints;
        y = this.getPolygon().ypoints;

        //Placer le centre du polygon à 0,0
        Point positionPolygon = this.getPolygon().getBounds().getLocation();
        this.getPolygon().translate(-positionPolygon.x - (int)this.getPolygon().getBounds().getWidth() / 2, -positionPolygon.y - (int)this.getPolygon().getBounds().getHeight() / 2);

        //Rotation
        for (int j = 0; j < 4; j++) {
            Point2D point2D = new Point2D.Double(x[j], y[j]);
            AffineTransform at = new AffineTransform();
            at.rotate(Math.toRadians(this.getAngle()));
            at.transform(point2D, point2D);
            rx[j] = (int) Math.round(point2D.getX());
            ry[j] = (int) Math.round(point2D.getY());
        }
        Polygon pr = new Polygon(rx, ry, 4);

        //Remettre le polygon à sa position initiale
        pr.translate(positionPolygon.x + (int)this.getPolygon().getBounds().getWidth() / 2, positionPolygon.y + (int)this.getPolygon().getBounds().getHeight() / 2);

        this.setPolygonAvecRotation(pr);
    }

    public void faireRotationAutourduCoin(){

        int[] x;
        int[] y;
        int[] rx = new int[4];
        int[] ry = new int[4];
        x = this.getPolygon().xpoints;
        y = this.getPolygon().ypoints;

        //Placer le centre du polygon à 0,0
        Point positionPolygon = this.getPolygon().getBounds().getLocation();
        this.getPolygon().translate(-positionPolygon.x - (int)this.getPolygon().getBounds().getWidth() / 2, -positionPolygon.y - (int)this.getPolygon().getBounds().getHeight() / 2);

        //Rotation
        for (int j = 0; j < 4; j++) {
            Point2D point2D = new Point2D.Double(x[j], y[j]);
            AffineTransform at = new AffineTransform();
            at.rotate(Math.toRadians(this.getAngle()));
            at.transform(point2D, point2D);
            rx[j] = (int) Math.round(point2D.getX());
            ry[j] = (int) Math.round(point2D.getY());
        }
        Polygon pr = new Polygon(rx, ry, 4);

        //Remettre le polygon à sa position initiale
        pr.translate(positionPolygon.x + (int)this.getPolygon().getBounds().getWidth() / 2, positionPolygon.y + (int)this.getPolygon().getBounds().getHeight() / 2);

        this.setPolygonAvecRotation(pr);
    }
}
