package ca.ulaval.glo2004.domaine.Table;

import ca.ulaval.glo2004.domaine.Utilitaires.Conversion;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.io.Serializable;

public class Poche implements Serializable {
    public static final double DIAMTRE_PAR_DEFAUT = Conversion.ConvertirEnImperial( 7.62);
    private Point position;
    private double diametre;
    private Ellipse2D forme;

    public Poche() {
    }

    public Poche(Point position){
        this.position = position;
        this.diametre = DIAMTRE_PAR_DEFAUT;
        this.forme = new Ellipse2D.Double(position.getX() - Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre/2)), position.getY() - Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre/2)), Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre)),  Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre)));

    }

    public double getDiametre() {
        return diametre;
    }

    public void setDiametre(double rayon) {
        this.diametre = rayon;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        int val = Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre/2));
        position.x = position.x-val;
        position.y = position.y-val;
        this.position = position;
        this.forme = new Ellipse2D.Double(position.x - val, position.y - val, Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre)),  Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre)));

    }

    public Ellipse2D getForme() { return forme;}

    public void setForme(Ellipse2D forme) {
        this.forme = forme;
    }

    }