package ca.ulaval.glo2004.domaine.Table;

import java.awt.*;
import java.io.Serializable;

public class Coin implements Serializable {
    private Rectangle hitbox;
    private Mur murGauche;
    private Mur murDroit;
    private Point prevPosition;

    public Coin(Rectangle hitbox, Mur murGauche, Mur murDroit) {
        this.hitbox = hitbox;
        this.murGauche = murGauche;
        this.murDroit = murDroit;
        Rectangle temp = (Rectangle) hitbox.clone();
        this.prevPosition = temp.getLocation();
    }

    public Coin(Rectangle hitbox, Mur murGauche, Mur murDroit, Point prevPosition) {
        this.hitbox = hitbox;
        this.murGauche = murGauche;
        this.murDroit = murDroit;
        this.prevPosition = prevPosition;
    }

    public Rectangle getHitbox() {
        return hitbox;
    }

    public void setHitbox(Rectangle hitbox) {
        this.hitbox = hitbox;
    }

    public Mur getMurGauche() {
        return murGauche;
    }

    public void setMurGauche(Mur murGauche) {
        this.murGauche = murGauche;
    }

    public Mur getMurDroit() {
        return murDroit;
    }

    public void setMurDroit(Mur murDroit) {
        this.murDroit = murDroit;
    }

    public Point getPrevPosition() {
        return prevPosition;
    }

    public void setPrevPosition(Point prevPosition) {
        this.prevPosition = prevPosition;
    }
}
