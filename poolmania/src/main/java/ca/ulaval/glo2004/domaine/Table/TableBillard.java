package ca.ulaval.glo2004.domaine.Table;

import ca.ulaval.glo2004.InterfaceUtilisateur.MainWindow;
import ca.ulaval.glo2004.domaine.Utilitaires.Conversion;
import ca.ulaval.glo2004.domaine.Utilitaires.Unite;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class TableBillard implements Serializable {

    private Dimension dimension;
    private Polygon forme;
    private Color couleur;
    private List<Poche> listePoches;
    private Boule[] listeBoules;
    private List<Mur> listeMurs;
    private List<Coin> listeCoin;
    private List<Obstacle> listeObstacles;
    private Queue queue;
    private double diametreBoules;
    private Stack<Object> undo;
    private Stack<Object> redo;
    private final int MAXIMUM_STACK_LENGTH = 999;
    private int nbMurs;
    private int longueur_murs;
    private int largeur_murs;

    public static final double DIAMETRE_PAR_DEFAUT = 2.25;

    public TableBillard(Dimension dimension, Polygon forme, Color couleur, List<Poche> listePoches, Boule[] listeBoules, List<Mur> listeMurs, Queue queue, List<Coin> coins, List<Obstacle> obstacles) {
        this.dimension = dimension;
        this.forme = forme;
        this.couleur = couleur;
        this.listePoches = listePoches;
        this.listeBoules = listeBoules;
        this.listeMurs = listeMurs;
        this.queue = queue;
        this.listeCoin = coins;
        this.listeObstacles = obstacles;
        this.undo = new Stack<>();
        this.redo = new Stack<>();
    }

    public TableBillard() {
        super();
        this.listeBoules = new Boule[16];
        this.listeMurs = new ArrayList<>();
        this.listePoches = new ArrayList<>();
        this.listeCoin = new ArrayList<>();
        this.listeObstacles = new ArrayList<>();
        this.diametreBoules = DIAMETRE_PAR_DEFAUT;
        this.undo = new Stack<>();
        this.redo = new Stack<>();
    }

    public TableBillard(TableBillard table) {
        this(table.getDimension(), table.getForme(), table.getCouleur(), table.getListePoches(), table.getListeBoules(), table.getListeMurs(), table.getQueue(), table.getListeCoins(), table.getListeObstacles());
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public Polygon getForme() {
        return forme;
    }

    public void setForme(Polygon forme) {
        this.forme = forme;
    }

    public Color getCouleur() {
        return couleur;
    }

    public void setCouleur(Color couleur) {
        this.couleur = couleur;
    }

    public List<Poche> getListePoches() {
        return listePoches;
    }

    public void setListePoches(List<Poche> listePoches) {
        this.listePoches = listePoches;
    }

    public void setListeCoins(List<Coin> listeCoin) {
        this.listeCoin = listeCoin;
    }

    public List<Coin> getListeCoins() {
        return listeCoin;
    }

    public Boule[] getListeBoules() {
        return listeBoules;
    }

    public void setListeBoules(Boule[] listeBoules) {
        this.listeBoules = listeBoules;
    }

    public List<Mur> getListeMurs() {
        return listeMurs;
    }

    public void setListeMurs(List<Mur> listeMurs) {
        this.listeMurs = listeMurs;
    }

    public Queue getQueue() {
        return queue;
    }

    public void setQueue(Queue queue) {
        this.queue = queue;
    }

    public boolean ExporterSVG() {
        return true;
    }

    public boolean ExporterXML() {
        return true;
    }

    public List<Obstacle> getListeObstacles() {
        return listeObstacles;
    }

    public void setListeObstacles(List<Obstacle> listeObstacles) {
        this.listeObstacles = listeObstacles;
    }

    public void Briser(int noMur) {
        //Valider le no du mur
        if (noMur != -1 && noMur < listeMurs.size()) {
            List<Coin> newList = new ArrayList<>(this.listeCoin);
            this.undo.push(newList);
            this.redo.clear();

            //Calcul de l'offset de la brisure du mur
            int[] x = listeMurs.get(noMur).getPolygon().xpoints.clone();
            int[] y = listeMurs.get(noMur).getPolygon().ypoints.clone();

            double offsetxlong = (x[1] + x[2]) / 2;
            double offsetylong = (y[1] + y[2]) / 2;

            double offsetxcourt = (x[0] + x[3]) / 2;
            double offsetycourt = (y[0] + y[3]) / 2;

            //Mise en place de la matrice de points pour la première moitié du mur
            int[] xPremMoit = listeMurs.get(noMur).getPolygon().xpoints.clone();
            int[] yPremMoit = listeMurs.get(noMur).getPolygon().ypoints.clone();

            xPremMoit[0] = (int) Math.round(offsetxcourt);
            yPremMoit[0] = (int) Math.round(offsetycourt);
            xPremMoit[1] = (int) Math.round(offsetxlong);
            yPremMoit[1] = (int) Math.round(offsetylong);

            //Mise en place de la matrice de points pour la deuxième moitié du mur
            int[] xDeuxMoit = listeMurs.get(noMur).getPolygon().xpoints.clone();
            int[] yDeuxMoit = listeMurs.get(noMur).getPolygon().ypoints.clone();

            xDeuxMoit[2] = (int) Math.round(offsetxlong);
            yDeuxMoit[2] = (int) Math.round(offsetylong);
            xDeuxMoit[3] = (int) Math.round(offsetxcourt);
            yDeuxMoit[3] = (int) Math.round(offsetycourt);

            //Génération des deux polygones
            Polygon premiereMoitie = new Polygon(xPremMoit, yPremMoit, 4);
            Polygon deuxiemeMoitie = new Polygon(xDeuxMoit, yDeuxMoit, 4);

            //Modification du mur actuel pour la première moitié
            getListeMurs().get(noMur).setPolygon(premiereMoitie);

            //Ajout d'un mur pour la 2e moitié
            listeMurs.add(noMur + 1, new MurExterne(0, new Color(102, 51, 0), deuxiemeMoitie, new Dimension(0, 0)));

            int moyx = Math.round(((xPremMoit[0] + xDeuxMoit[2]) / 2) - (15 / 2));
            int moyy = Math.round(((yPremMoit[1] + yDeuxMoit[3]) / 2) - (15 / 2));

            Rectangle rectangle = new Rectangle();
            rectangle.setRect(moyx, moyy, 15, 15);
            listeCoin.add(new Coin(rectangle, listeMurs.get(noMur), listeMurs.get(noMur + 1)));
        }
    }

    public Object trouverElementClic(Point mousePoint) {
        for (Poche poche : listePoches) {
            if (poche != null && poche.getForme().contains(mousePoint)) {
                return poche;
            }
        }
        for (Boule boule : listeBoules) {
            if (boule != null && boule.getForme().contains(mousePoint)) {
                return boule;
            }
        }
        for (Coin coin : listeCoin) {
            Shape shape = coin.getHitbox();
            if (shape.contains(mousePoint)) {
                return coin;
            }
        }
        for (Obstacle obstacle : listeObstacles) {
            Shape shape = obstacle.getPolygonAvecRotation();
            if (shape.contains(mousePoint)) {
                return obstacle;
            }
        }
        for (Mur mur : listeMurs) {
            Shape shape = mur.getPolygon();
            if (shape.contains(mousePoint)) {
                return mur;
            }
        }
        return null;
    }

    public void addMurToList(Mur mur) {
        this.listeMurs.add(mur);
    }

    public void addPocheToList(Poche poche) {
        this.listePoches.add(poche);
    }

    public double getDiametreBoules() {
        return diametreBoules;
    }

    public void setDiametreBoules(double diametreBoules) {
        if (this.diametreBoules != diametreBoules) {
            double newList[] = {this.diametreBoules};
            this.undo.push(newList);
            this.redo.clear();

            this.diametreBoules = diametreBoules;
            this.recalculerFormeBoules();
        }
    }

    private void recalculerFormeBoules() {
        for (Boule boule : listeBoules) {
            if (boule != null) {
                Rectangle2D frame = new Rectangle2D.Double(boule.getPosition().getX(),
                        boule.getPosition().getY(), Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(getDiametreBoules())), Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(getDiametreBoules())));
                boule.getForme().setFrame(frame);
            }
        }
    }

    public Coin verifierCollisionMur(Obstacle obstacle) {

        //Vérifier que l'obstacle n'a pas déjà de collision
        if (obstacle.getCoinCollision() != null) {
            return null;
        }

        for (Obstacle obstacle2 : listeObstacles) {
            if (obstacle2 != obstacle) {

                Area area1 = new Area(obstacle.getPolygonAvecRotation());
                Area area2 = new Area(obstacle2.getPolygonAvecRotation());

                area1.intersect(area2);
                if (!area1.isEmpty()) {
                    System.out.println("Collision !!");

                    Point centreCollision = new Point((int) area1.getBounds().getCenterX(), (int) area1.getBounds().getCenterY());
                    Rectangle coinHitbox = new Rectangle((int) centreCollision.getX(), (int) centreCollision.getY(), 10, 10);
                    Coin coin = new Coin(coinHitbox, obstacle, obstacle2);
                    obstacle.setCoinCollision(coin);
                    obstacle2.setCoinCollision(coin);
                    return coin;
                }
            }
        }
        return null;
    }

    public Boule verifierCollisionBoules(Boule boule) {
        for (Boule boule1 : listeBoules) {
            if (boule1 != boule && boule1 != null && boule != null) {

                if (boule.getForme().intersects(boule1.getForme().getBounds2D())) {
                    return boule1;
                }
            }
        }
        return null;
    }

    public boolean verifierCollisionPoche(Boule boule) {
        for (Poche poche : listePoches) {
            if (boule.getForme().intersects(poche.getForme().getBounds2D())) {
                return true;
            }
        }
        return false;
    }

    public Mur verifierCollisionBouleMur(Boule boule) {
        List<Mur> listeTotMurs = listeMurs;
        for (Mur mur : listeObstacles) {
            listeTotMurs.add(mur);
        }
        for (Mur mur : listeTotMurs) {
            if (boule.getForme().intersects(mur.getPolygon().getBounds2D())) {
                return mur;
            }
        }
        return null;
    }

    public void deleteMurs() {
        listeMurs.clear();
        listeCoin.clear();
    }

    public void pushPoches() {
        List<Poche> newList = new ArrayList<>(this.listePoches);
        this.undo.push(newList);
        this.redo.clear();
    }

    public void undo() {
        if (!this.undo.isEmpty() && this.undo.size() <= MAXIMUM_STACK_LENGTH) {
            Object obj = this.undo.pop();
            if (obj instanceof Boule[]) {
                Boule[] newList = new Boule[this.listeBoules.length];
                System.arraycopy(this.listeBoules, 0, newList, 0, newList.length);
                this.redo.push(newList);

                this.listeBoules = (Boule[]) obj;
            } else if (obj instanceof List && !((List) obj).isEmpty()) {
                if (((List) obj).get(0) instanceof MurExterne) {
                    List<Mur> newList = new ArrayList<>(this.listeMurs);
                    this.redo.push(newList);

                    this.listeMurs = (List<Mur>) obj;
                } else if (((List) obj).get(0) instanceof Poche) {
                    List<Poche> newList = new ArrayList<>(this.listePoches);
                    this.redo.push(newList);

                    this.listePoches = (List<Poche>) obj;
                } else if (((List) obj).get(0) instanceof Obstacle) {
                    List<Obstacle> newList = new ArrayList<>(this.listeObstacles);
                    this.redo.push(newList);

                    this.listeObstacles = (List<Obstacle>) obj;
                } else if (((List) obj).get(0) instanceof Coin) {
                    List<Coin> newList = new ArrayList<>(this.listeCoin);
                    this.redo.push(newList);

                    this.listeCoin = (List<Coin>) obj;
                }
            } else if (obj instanceof int[]) {
                int[] oldTable = new int[]{this.nbMurs, this.longueur_murs, this.largeur_murs};
                this.redo.push(oldTable);

                int[] table = (int[]) obj;
                this.modifyTable(table[0], table[1], table[2], false);
            } else if (obj instanceof double[]) {
                double newList[] = {this.diametreBoules};
                this.redo.push(newList);

                double[] diametre = (double[]) obj;
                this.diametreBoules = diametre[0];
                this.recalculerFormeBoules();
            }
        }
    }

    public void redo() {
        if (!this.redo.isEmpty()) {
            Object obj = this.redo.pop();
            if (obj instanceof Boule[]) {
                Boule[] newList = new Boule[this.listeBoules.length];
                System.arraycopy(this.listeBoules, 0, newList, 0, newList.length);
                this.undo.push(newList);

                this.listeBoules = (Boule[]) obj;
            } else if (obj instanceof List && !((List) obj).isEmpty()) {
                if (((List) obj).get(0) instanceof MurExterne) {
                    List<Mur> newList = new ArrayList<>(this.listeMurs);
                    this.undo.push(newList);

                    this.listeMurs = (List<Mur>) obj;
                } else if (((List) obj).get(0) instanceof Poche) {
                    List<Poche> newList = new ArrayList<>(this.listePoches);
                    this.undo.push(newList);

                    this.listePoches = (List<Poche>) obj;
                } else if (((List) obj).get(0) instanceof Obstacle) {
                    List<Obstacle> newList = new ArrayList<>(this.listeObstacles);
                    this.undo.push(newList);

                    this.listeObstacles = (List<Obstacle>) obj;
                } else if (((List) obj).get(0) instanceof Coin) {
                    List<Coin> newList = new ArrayList<>(this.listeCoin);
                    this.undo.push(newList);

                    this.listeCoin = (List<Coin>) obj;
                }
            } else if (obj instanceof int[]) {
                int[] oldTable = new int[]{this.nbMurs, this.longueur_murs, this.largeur_murs};
                this.undo.push(oldTable);

                int[] table = (int[]) obj;
                this.modifyTable(table[0], table[1], table[2], false);
            } else if (obj instanceof double[]) {
                double newList[] = {this.diametreBoules};
                this.undo.push(newList);

                double[] diametre = (double[]) obj;
                this.diametreBoules = diametre[0];
                this.recalculerFormeBoules();
            }
        }
    }

    public void ajouterBouleNumerote(Point clic) {
        //trouver la boule la plus proche
        for (int i = 1; i < this.listeBoules.length; i++) {
            if (this.listeBoules[i] == null) {
                this.pushAndClear();
                this.listeBoules[i] = new BouleNumerote(new Point((clic.x), (clic.y)), i, diametreBoules, this);
                return;
            }
        }
    }

    public void ajouterBouleBlanche(Point clic) {
        if (this.listeBoules[0] == null) {
            this.pushAndClear();
            this.listeBoules[0] = new Boule(new Point(clic.x, clic.y), true, diametreBoules, this);
        }
    }

    public void removeWhiteBall() {
        this.pushAndClear();
        this.listeBoules[0] = null;
    }

    //To do before each actions (add, remove, deplace)
    private void pushAndClear() {
        Boule[] newList = new Boule[this.listeBoules.length];
        System.arraycopy(this.listeBoules, 0, newList, 0, newList.length);
        this.undo.push(newList);
        this.redo.clear();
    }

    public void removeBall(Object elementSelectionne) {
        for (int i = 1; i < this.listeBoules.length; i++) {
            if (this.listeBoules[i] == elementSelectionne) {
                this.pushAndClear();
                this.listeBoules[i] = null;
            }
        }
    }

    public void supprimerElement(TypeElementTable typeElement, Object elementSelectionne) {
        switch (typeElement) {
            case OBSTACLE:
                if (this.listeObstacles.contains(elementSelectionne)) {
                    this.enleverUnObstacle(elementSelectionne);
                }
                break;
            case BOULE_NUMEROTE:
                this.removeBall(elementSelectionne);
                break;
            case BOULE_BLANCHE:
                if (this.listeBoules[0] == elementSelectionne) {
                    this.removeWhiteBall();
                }
                break;
            case POCHE:
                if (this.listePoches.contains(elementSelectionne)) {
                    this.removePoche(elementSelectionne);
                }
                break;
            default:
                break;
        }
    }

    private void removePoche(Object elementSelectionne) {
        List<Poche> newList = new ArrayList<>(this.listePoches);
        this.undo.push(newList);
        this.redo.clear();

        this.listePoches.remove(elementSelectionne);
    }

    private void enleverUnObstacle(Object elementSelectionne) {
        List<Obstacle> newList = new ArrayList<>(this.listeObstacles);
        this.undo.push(newList);
        this.redo.clear();

        this.listeObstacles.remove(elementSelectionne);
    }

    public void ajouterUnObstacle(Point mousePoint) {
        List<Obstacle> newList = new ArrayList<>(this.listeObstacles);
        this.undo.push(newList);
        this.redo.clear();
        System.err.println(newList);

        Color couleur = new Color(102, 51, 0);
        int[] x = {(int) mousePoint.getX(), (int) mousePoint.getX(), (int) mousePoint.getX() + Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(Obstacle.LONGUEUR_PAR_DEFAUT)), (int) mousePoint.getX() + Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(Obstacle.LONGUEUR_PAR_DEFAUT))};
        int[] y = {(int) mousePoint.getY(), (int) mousePoint.getY() + Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(Obstacle.HAUTEUR_PAR_DEFAUT)), (int) mousePoint.getY() + Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(Obstacle.HAUTEUR_PAR_DEFAUT)), (int) mousePoint.getY()};
        Polygon polygon = new Polygon(x, y, 4);
        polygon.translate(-Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(Obstacle.LONGUEUR_PAR_DEFAUT / 2)), -Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(Obstacle.HAUTEUR_PAR_DEFAUT / 2)));
        Dimension dimension = new Dimension((int) Obstacle.LONGUEUR_PAR_DEFAUT, (int) Obstacle.HAUTEUR_PAR_DEFAUT);
        Obstacle obstacle = new Obstacle(0, couleur, polygon, dimension);
        this.verifierCollisionMur(obstacle);
        this.listeObstacles.add(obstacle);
    }

    void ajouterUnePoche(Point mousePoint) {
        List<Poche> newList = new ArrayList<>(this.listePoches);
        this.undo.push(newList);
        this.redo.clear();

        this.listePoches.add(new Poche(mousePoint));
    }

    public void saveTable() {
        String result = JOptionPane.showInputDialog("Enter a file name");
        if (!result.isEmpty()) {
            try {
                FileOutputStream fileOut = new FileOutputStream("saves/" + result + ".ser");
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                List list = new ArrayList<>();
                list.add(this.listeMurs);
                list.add(this.listePoches);
                list.add(this.listeCoin);
                Point[] points = new Point[this.listeBoules.length];
                for (int i = 0; i < points.length; i++) {
                    if (this.listeBoules[i] == null) {
                        points[i] = null;
                    } else {
                        points[i] = this.listeBoules[i].getPosition();
                    }
                }
                list.add(points);
                list.add(this.nbMurs);
                list.add(this.longueur_murs);
                list.add(this.largeur_murs);
                out.writeObject(list);
                out.close();
                fileOut.close();
                System.out.printf("Serialized data is saved in /saves/table.ser");
            } catch (IOException ex) {
                Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void loadTable() {
        try {
            JFileChooser chooser = new JFileChooser();
            //chooser.setCurrentDirectory(chooser.getCurrentDirectory());
            File file = null;
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Ser file", "ser");
            chooser.setCurrentDirectory(new File(System.getProperty("user.home") + System.getProperty("file.separator") + "PoolMania/H22-GLO-Equipe5/poolmania/saves"));
            chooser.setFileFilter(filter);
            int returnValue = chooser.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                file = chooser.getSelectedFile();
            }
            if (file == null || !file.getName().toLowerCase().endsWith(".ser")) {
                return;
            }
            try ( FileInputStream fileIn = new FileInputStream("saves/" + file.getName())) {
                ObjectInputStream in = new ObjectInputStream(fileIn);
                ArrayList woi = (ArrayList) in.readObject();
                List<Mur> murs = (List<Mur>) woi.get(0);
                List<Poche> poches = (List<Poche>) woi.get(1);
                //List<Coin> coins = (List<Coin>) woi.get(2);
                Point[] boules = (Point[]) woi.get(3);
                int mursNb = (int) woi.get(4);
                int longueur = (int) woi.get(5);
                int largeur = (int) woi.get(6);
                this.creerTable(mursNb, longueur, largeur, boules, murs, poches);
                in.close();
                this.undo.clear();
                this.redo.clear();
            }
        } catch (IOException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException c) {
            System.out.println("Class not found");
        }
    }

    public void ajouterBoulesInitiales(Point[] boulesInitiales) {
        if (boulesInitiales[0] != null) {
            this.listeBoules[0] = new Boule(boulesInitiales[0], true, diametreBoules, this);
        }
        for (int i = 1; i < boulesInitiales.length; i++) {
            if (boulesInitiales[i] != null) {
                this.listeBoules[i] = new BouleNumerote(boulesInitiales[i], i, diametreBoules, this);
            }
        }
    }

    public void creerTable(int nombreMurs, int longueur, int largeur, Point[] boulesInitiales, List<Mur> mursInitiaux, List<Poche> pochesInitiales) {
        this.longueur_murs = longueur;
        this.largeur_murs = largeur;
        if (mursInitiaux != null) {
            this.listeMurs = mursInitiaux;
            this.setFondTable(mursInitiaux.size());
            nbMurs = mursInitiaux.size();
        } else {
            this.createWalls(largeur, longueur, nombreMurs);
        }
        this.ajouterCoins();
        if (pochesInitiales != null) {
            this.listePoches = pochesInitiales;
        } else {
            this.ajouterPochesCoinsInterieurs();
        }
        if (boulesInitiales != null) {
            this.ajouterBoulesInitiales(boulesInitiales);
        }
    }

    public void setFondTable(int nombreMurs) {
        //Pour le fond de la table
        int[] fondx = new int[nombreMurs * 2];
        int[] fondy = new int[nombreMurs * 2];

        //permet d'aller chercher tous les coins intérieur des murs externes de la table en X et Y
        int cpt = 0;
        for (int i = 0; i < nombreMurs * 2; i += 2) {
            fondx[i] = this.listeMurs.get(cpt).getPolygon().xpoints[3];
            fondx[i + 1] = this.listeMurs.get(cpt).getPolygon().xpoints[0];

            fondy[i] = this.listeMurs.get(cpt).getPolygon().ypoints[3];
            fondy[i + 1] = this.listeMurs.get(cpt).getPolygon().ypoints[0];
            cpt++;
        }

        Polygon fond = new Polygon(fondx, fondy, nombreMurs * 2);

        this.setForme(fond);
    }

    public void modifyTable(int nombreMurs, int longueur, int largeur, boolean pushOldTable) {
        if (this.listeMurs.isEmpty()) {
            throw new Error("La table n'est pas initialisée.");
        }
        if (pushOldTable) {
            int[] table = new int[]{this.nbMurs, this.longueur_murs, this.largeur_murs};
            this.undo.push(table);
            this.redo.clear();
        }
        this.longueur_murs = longueur;
        this.largeur_murs = largeur;

        this.deleteMurs();
        this.createWalls(largeur, longueur, nombreMurs);
        this.ajouterCoins();
    }

    public Polygon getPointsCotes(double largeurMurs, double longueurMur, int nbMurs) {

        //Calcul de l'angle interne entre chaques murs
        double angle = 0;
        if (nbMurs > 2) {
            //Si c'est un polygone
            //Pour connaitre l'angle individuel des polygones
            angle = (((nbMurs - 2) * 180) / nbMurs) / 2;
            //angle = 180-angle-90;
        } else {
            throw new RuntimeException("Formulaire invalide");
            //Si forme invalide
        }

        int[] x = new int[4];
        int[] y = new int[4];

        //Valeur arrondie des longueurs des côtés du polygone
        int largeurMurIntVal = Math.round(Float.parseFloat(String.valueOf(largeurMurs)));
        int longeurMurIntVal = Math.round(Float.parseFloat(String.valueOf(longueurMur)));
        double test = Math.tan(Math.toRadians(angle));
        double valeurOffset = largeurMurs / test;
        int offsetLongueurTop = Math.round(Float.parseFloat(String.valueOf(valeurOffset)));

        //Coordonnées du polygone
        x[0] = offsetLongueurTop;
        y[0] = 0;

        x[1] = 0;
        y[1] = largeurMurIntVal;

        x[2] = longeurMurIntVal;
        y[2] = largeurMurIntVal;

        x[3] = longeurMurIntVal - offsetLongueurTop;
        y[3] = 0;

        //Créer le polygone
        Polygon p = new Polygon(x, y, 4);

        return p;
    }

    private void createWalls(int largeur, int longueur, int nombreMurs) {
        Polygon p = this.getPointsCotes(largeur, longueur, nombreMurs);
        for (int i = 0; i < nombreMurs; i++) {
            double angle = i * ((180 - (((nombreMurs - 2) * 180) / nombreMurs)));

            //rotation des points
            //Source de la solution: https://stackoverflow.com/questions/13347503/how-to-apply-transformation-to-polygon-object-in-java
            int[] x = new int[4];
            int[] y = new int[4];
            int[] rx = new int[4];
            int[] ry = new int[4];
            x = p.xpoints;
            y = p.ypoints;

            for (int j = 0; j < 4; j++) {
                Point2D point2D = new Point2D.Double(x[j], y[j]);
                AffineTransform at = new AffineTransform();
                at.rotate(Math.toRadians(angle), (p.getBounds().width) / 2, -this.anchory(nombreMurs, p.getBounds().width, largeur) / 2);
                at.transform(point2D, point2D);
                rx[j] = (int) Math.round(point2D.getX());
                ry[j] = (int) Math.round(point2D.getY());
            }
            Polygon pr = new Polygon(rx, ry, 4);
            //tableBillard
            pr.translate(350, 450);

            this.listeMurs.add(new MurExterne(0, new Color(102, 51, 0), pr, new Dimension(longueur, largeur)));
        }
        setFondTable(nombreMurs);
        this.nbMurs = nombreMurs;
    }

    private double anchory(int nbMurs, double longueurMurs, double largeurMur) {
        //Calcul de l'angle pour trouver la hauteur d'un triangle du polygone
        double offsetAngle = (((nbMurs - 2) * 180) / nbMurs) / 2;
        double testoffset = Math.tan(Math.toRadians(offsetAngle));
        double offsetLongueurTop = largeurMur / testoffset;

        if (nbMurs == 4) {
            //Si la forme est un carré
            return (longueurMurs - (offsetLongueurTop) / 2) - (largeurMur);
        }

        //Autrement
        return (int) ((longueurMurs - (offsetLongueurTop) / 2) / Math.tan(Math.toRadians(180 / nbMurs)) - (largeurMur));

    }

    private void ajouterCoins() {
        for (int i = 0; i < this.listeMurs.size() - 1; i++) {
            if (i == 0) {

                Rectangle rectangle = new Rectangle();
                int[] x = this.listeMurs.get(i).getPolygon().xpoints.clone();
                int[] y = this.listeMurs.get(i).getPolygon().ypoints.clone();

                int[] x2 = this.listeMurs.get(this.listeMurs.size() - 1).getPolygon().xpoints.clone();
                int[] y2 = this.listeMurs.get(this.listeMurs.size() - 1).getPolygon().ypoints.clone();

                int moyx = Math.round((x[3] + x2[1]) / 2);
                int moyy = Math.round((y[2] + y2[0]) / 2);

                moyx = moyx - (15 / 2);
                moyy = moyy - (15 / 2);

                rectangle.setRect(moyx, moyy, 15, 15);
                this.listeCoin.add(new Coin(rectangle, this.listeMurs.get(i), this.listeMurs.get(this.listeMurs.size() - 1)));

                int[] x3 = this.listeMurs.get(i + 1).getPolygon().xpoints.clone();
                int[] y3 = this.listeMurs.get(i + 1).getPolygon().ypoints.clone();

                int moyx3 = Math.round((x[0] + x3[2]) / 2);
                int moyy3 = Math.round((y[1] + y3[3]) / 2);

                moyx3 = moyx3 - (15 / 2);
                moyy3 = moyy3 - (15 / 2);

                Rectangle rectangle3 = new Rectangle();
                rectangle3.setRect(moyx3, moyy3, 15, 15);
                this.listeCoin.add(new Coin(rectangle3, this.listeMurs.get(i), this.listeMurs.get(i + 1)));
            } else {
                Rectangle rectangle = new Rectangle();
                int[] x = this.listeMurs.get(i).getPolygon().xpoints.clone();
                int[] y = this.listeMurs.get(i).getPolygon().ypoints.clone();

                int[] x2 = this.listeMurs.get(i + 1).getPolygon().xpoints.clone();
                int[] y2 = this.listeMurs.get(i + 1).getPolygon().ypoints.clone();

                int moyx = Math.round((x[0] + x2[2]) / 2);
                int moyy = Math.round((y[1] + y2[3]) / 2);

                moyx = moyx - (15 / 2);
                moyy = moyy - (15 / 2);

                rectangle.setRect(moyx, moyy, 15, 15);
                this.listeCoin.add(new Coin(rectangle, this.listeMurs.get(i), this.listeMurs.get(i + 1)));

            }
        }
    }

    private void ajouterPochesCoinsInterieurs() {
        List<Coin> coins = this.listeCoin;
        Point centroid = trouverCentroidTable();

        for (Coin coin : coins) {
            Point point = new Point();
            int offsetX = 0;
            int offsetY = 0;
            point.setLocation(coin.getHitbox().getX(), coin.getHitbox().getY());
            if (point.getX() > centroid.getX()) {
                if (point.getX() < centroid.getX() + 5) {
                    offsetX = 0;
                } else {
                    offsetX = -35;
                }
            } else if (point.getX() == centroid.getX()) {
                offsetX = 0;
            } else {
                if (point.getX() > centroid.getX() - 5) {
                    offsetX = 0;
                } else {
                    offsetX = +40;
                }
            }

            if (point.getY() > centroid.getY()) {
                if (point.getY() < centroid.getY() + 5) {
                    offsetY = 0;
                } else {
                    offsetY = -25;
                }
            } else if (point.getY() == centroid.getY()) {
                offsetY = 0;
            } else {
                if (point.getY() > centroid.getY() - 5) {
                    offsetY = 0;
                } else {
                    offsetY = +40;
                }
            }
            point.translate(offsetX, offsetY);

            Poche poche = new Poche(point);
            this.listePoches.add(poche);
        }
    }

    private Point trouverCentroidTable() {
        //Source de la solution: https://stackoverflow.com/questions/18591964/how-to-calculate-centroid-of-an-arraylist-of-points
        List<Coin> coins = this.listeCoin;
        int centroidX = 0;
        int centroidY = 0;
        for (Coin coin : coins) {
            centroidX += coin.getHitbox().getX();
            centroidY += coin.getHitbox().getY();
        }
        return new Point(centroidX / coins.size(), centroidY / coins.size());
    }

    public void miseAJourValeurs(ElementDTO elementDTO, Unite unite, TypeElementTable typeElement, Object elementSelectionne) {

    }

    private Double changementUnites(Double valeur, Unite unite) {
        if (valeur == null) {
            return null;
        }
        //Si l'interface utilisateur est en centimetre pas besoin de convertir
        /*if(unite == Unite.POUCES) {
            valeur = Conversion.ConvertirEnImperial(valeur);
        }*/
        return valeur;
    }
}
