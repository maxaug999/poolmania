package ca.ulaval.glo2004.domaine.Drawing;

import java.awt.*;

public class GrilleMagnetique {
    private double echelleGrille;
    private double largeurGrille;
    private double hauteurGrille;
    private boolean estVisible;
    private Color couleur;

    public GrilleMagnetique(double largeurGrille, double hauteurGrille, double echelleGrille, boolean estVisible) {
        this.largeurGrille = largeurGrille;
        this.hauteurGrille = hauteurGrille;
        this.echelleGrille = echelleGrille;
        this.estVisible = estVisible;
    }

    public double getEchelleGrille() {
        return echelleGrille;
    }

    public void setEchelleGrille(double echelleGrille) {
        this.echelleGrille = echelleGrille;
    }

    public double getLargeurGrille() {
        return largeurGrille;
    }

    public void setLargeurGrille(double largeurGrille) {
        this.largeurGrille = largeurGrille;
    }

    public double getHauteurGrille() {
        return hauteurGrille;
    }

    public void setHauteurGrille(double hauteurGrille) {
        this.hauteurGrille = hauteurGrille;
    }

    public boolean estVisible() {
        return estVisible;
    }

    public void estVisible(boolean estVisible) {
        this.estVisible = estVisible;
    }

    public Color getCouleur() {
        return couleur;
    }

    public void setCouleur(Color couleur) {
        this.couleur = couleur;
    }

    public Point snapGrille(Point positionCurseur) {
        Point snapPoint = new Point();
        int echelle = (int)this.echelleGrille;
        int xCourant = (int)positionCurseur.getX();
        int yCourant = (int)positionCurseur.getY();
        int moduloX = xCourant % echelle;
        int moduloY = yCourant % echelle;
        int difference;


        if (moduloX > echelle/2) {
            difference = echelle - moduloX;
            snapPoint.x = xCourant + difference;
        }else if (moduloX <= echelle/2){
            snapPoint.x = xCourant - moduloX;
        }
        if (moduloY > echelle/2) {
            difference = echelle - moduloY;
            snapPoint.y = yCourant + difference;
        }else if (moduloY <= echelle/2){
            snapPoint.y = yCourant - moduloY;
        }

        return snapPoint;
    }
}
