package ca.ulaval.glo2004.domaine.Table;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;

public class BouleNumerote extends Boule {

    private final int numero;

    public BouleNumerote(Point position, int numero, double diametreBoules, TableBillard tableBillard) {
        super(position, false, diametreBoules, tableBillard);
        this.numero = numero;
        this.setImage(this.chercherImageBoule(numero));
    }

    public int getNumero() {
        return this.numero;
    }

    public BufferedImage chercherImageBoule(int numero) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(this.getClass().getResource("/images/" + numero + ".png"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return image;
    }
}
