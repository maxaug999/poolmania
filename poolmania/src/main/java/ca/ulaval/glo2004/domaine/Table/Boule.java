package ca.ulaval.glo2004.domaine.Table;

import ca.ulaval.glo2004.domaine.Utilitaires.Conversion;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.Serializable;

import static java.lang.Math.sqrt;

public class Boule implements Serializable {

    private class Vecteur {
        //angle en degres (360)
        private double angle;
        private double force;

        public Vecteur(double angle, double force) {
            this.angle = angle;
            this.force = force;
        }
    }

    //public double diametre;
    private Point position;
    private Point2D.Double mouvement;
    private boolean estBouleBlanche;
    private BufferedImage image;
    private Ellipse2D forme;
    private Vecteur direction;
    private double diametre;
    private double FRICTION = 0.95d;

    public Boule() {
    }

    public Boule(Point position, boolean estBouleBlanche, double diametre, TableBillard tableBillard) {
        this.position = position;
        this.estBouleBlanche = estBouleBlanche;
        this.image = chercherImageBoule();
        this.diametre = diametre;
        this.forme = new Ellipse2D.Double(position.getX() - Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre)) /2, position.getY() - Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre))/2, Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre)),  Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre)));
        this.direction = new Vecteur(0, 0);
        this.mouvement = new Point2D.Double(0, 0);
       // this.tableBillard = tableBillard;
    }
    
    public BufferedImage chercherImageBoule() {
        BufferedImage image = null;
        try {
            if (this.isEstBouleBlanche()) {
                image = ImageIO.read(this.getClass().getResource("/images/blanche.png"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return image;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
        this.forme = new Ellipse2D.Double(getPosition().getX() - Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre)) /2, getPosition().y - Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre))/2, Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre)),  Conversion.CentimetreVersPixel(Conversion.ConvertirEnMetrique(diametre)));
    }

    public boolean isEstBouleBlanche() {
        return estBouleBlanche;
    }

    public void setEstBouleBlanche(boolean estBouleBlanche) {
        this.estBouleBlanche = estBouleBlanche;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public Ellipse2D getForme() {
        return forme;
    }

    public void setForme(Ellipse2D forme) {
        this.forme = forme;
    }

    public Vecteur getDirection() {
        return direction;
    }

    public void setDirection(Vecteur direction) {
        this.direction = direction;
    }

    public Point2D.Double GetMouvement() {
        return mouvement;
    }

    public void deplacer() {
        double x = mouvement.getX();
        double y = mouvement.getY();
        x *= FRICTION;
        y *= FRICTION;

        this.SetMouvement(new Point2D.Double(x, y));
        this.setPosition(new Point(this.getPosition().x + (int) Math.round(x), this.getPosition().y + (int) Math.round(y)));
    }

    public void SetMouvement(final Point2D.Double mouvement) {
        this.mouvement = mouvement;
    }

    public void HandleBallCollision(Boule otherBall) {
        //Vecteur entre les 2 centres des balles
        Point vecteurUnitaire = CalculateUnitaryVector(new Point(otherBall.getPosition().x - this.getPosition().x,otherBall.getPosition().y - this.getPosition().y));
        //Droite tangente du vecteur unitaire
        Point tangenteUnitaire = new Point(-vecteurUnitaire.y, vecteurUnitaire.x);

        Point vitesseNormale1 = new Point((vecteurUnitaire.x * otherBall.getPosition().x)*vecteurUnitaire.x, (vecteurUnitaire.y * otherBall.getPosition().y) * vecteurUnitaire.y);
        Point vitesseTangente1 = new Point((tangenteUnitaire.x * this.getPosition().x)*tangenteUnitaire.x, (tangenteUnitaire.y * this.getPosition().y) * tangenteUnitaire.y);
        Point vitesseNormale2 = new Point((vecteurUnitaire.x * this.getPosition().x)*vecteurUnitaire.x, (vecteurUnitaire.y * this.getPosition().y) * vecteurUnitaire.y);
        Point vitesseTangente2 = new Point((tangenteUnitaire.x * otherBall.getPosition().x)*tangenteUnitaire.x, (tangenteUnitaire.y * otherBall.getPosition().y) * tangenteUnitaire.y);

        //this.SetMovement(new Point(vitesseNormale1.x + vitesseTangente1.x, vitesseNormale1.y + vitesseTangente1.y));
        //otherBall.SetMovement(new Point(vitesseNormale2.x + vitesseTangente2.x, vitesseNormale2.y + vitesseTangente2.y));

        this.SetMouvement(new Point2D.Double((vitesseNormale1.x + vitesseTangente1.x), vitesseNormale1.y + vitesseTangente1.y));
        otherBall.SetMouvement(new Point2D.Double(vitesseNormale2.x + vitesseTangente2.x, vitesseNormale2.y + vitesseTangente2.y));
    }

    Point CalculateUnitaryVector(Point normal) {
        float pente = (float) sqrt(normal.x * normal.x + normal.y * normal.y);
        //Si 0, ça prov0que une division par 0.
        if (pente != 0.0f) {
            return new Point(Math.round(normal.x / pente), Math.round(normal.y / pente));
        }
        return new Point(0, 0);
    }

}

