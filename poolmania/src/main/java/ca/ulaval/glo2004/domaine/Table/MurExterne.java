package ca.ulaval.glo2004.domaine.Table;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

public class MurExterne extends Mur implements Serializable {

    public MurExterne(double angle, Color couleur, Polygon forme, Dimension dimension) {
        super(angle, couleur, forme, dimension);
    }

    public boolean validerDoubleConnexion(Mur mur) {
        return false;
    }
}
